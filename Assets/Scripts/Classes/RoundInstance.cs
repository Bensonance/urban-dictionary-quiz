﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoundInstance {

    public string RoundName;
    public WORD_CATEGORY RoundFocusCategory;
    [Range(1,10)]
    public int WordsPerRound;
    [Range(1, 5)]
    public float PointsModifier;
    public string RoundPublicName;

    [Header("Round Options")]
    public bool RandomiseCategory, ChooseCategory;

    public WORD_CATEGORY RandomiseFocusCategory(List<WORD_CATEGORY> toAvoid = null)
    {
        System.Array valueHolder = UtilityFunctions.GetEnumTypeArrayValues(typeof(WORD_CATEGORY));
        int valueCount = UtilityFunctions.GetEnumTypeValueCount(typeof(WORD_CATEGORY));
        WORD_CATEGORY holder = (WORD_CATEGORY)valueHolder.GetValue(Random.Range(0, valueCount));
        if (toAvoid != null && toAvoid.Count + 1 >= valueCount)
        { 
            Debug.LogError("All categories have been used");
            return WORD_CATEGORY.ALL;
        }

        //Should avoid All category + any used categories before
        while(holder == WORD_CATEGORY.ALL || (toAvoid != null && UtilityFunctions.IsEqualToAny<WORD_CATEGORY>(holder, toAvoid.ToArray())))
        {
            holder = (WORD_CATEGORY)valueHolder.GetValue(Random.Range(0, valueCount - 1));
        }
        Debug.Log("Holder: " + holder.ToString());
        RoundFocusCategory = holder;
        return RoundFocusCategory;
    }


}
