﻿public enum GAME_MODE
{
    SINGLE,
    TWO
}

public enum PLAYER_IN_CONTROL
{
    ALL,
    ONE,
    TWO,
    NONE
}

public enum WORD_CATEGORY
{
    ALL,
    SEX,
    MUSIC,
    WORK,
    NAMES,
    SOUTH_AFRICA,
    FOOD,
    INTERNET
}


