﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nyamakop;
using UnityEngine.UI;
using DG.Tweening;

public class UrbanDictionaryWordManager : Singleton<UrbanDictionaryWordManager> {

    public Text WordDescription;
    public Text[] WordPositions;

    public List<UrbanWordClass> Words = new List<UrbanWordClass>();
    private List<UrbanWordClass> _trackedWords = new List<UrbanWordClass>();
    private UrbanWordClass _currentWord;

    public int CorrectScore, IncorrectScore;

    public delegate void IncorrectWord();
    public delegate void CorrectWord();

    public IncorrectWord WrongWordBrah;
    public CorrectWord RightWordBrah;

    private float _textUnfoldTime = 5f;

    private Utility_EffectsStack _effects;
    private UtilityFunctions.UnfoldTextEndCall _endCall;
    private int _correctWordPosition;

    public static float WrongWordFeedbackTime = 3f, RightWordFeedbackTime = 3f;

    private Tween _wordUnfoldTween;
    public bool DescriptionFinishedUnfolding
    {
        get
        {
            return !_wordUnfoldTween.IsPlaying() || _wordUnfoldTween.IsComplete();
        }
    }


    public ScoreMananger Player1Score, Player2Score, QuestionCounter;

    [Header("Rounds")]
    public RoundInstance[] Rounds;
    private int _currentRound = 0;
    private WORD_CATEGORY _currentCategory;
    private List<WORD_CATEGORY> _previouslyUsed = new List<WORD_CATEGORY>();

    public RoundInstance CurrentRound
    {
        get
        {
            return Rounds[_currentRound];
        }
    }
    private WORD_CATEGORY[] _categoryChoices;

    // Use this for initialization

    private void Awake()
    {
        base.Awake();

    }

    void Start() {

        _effects = GetComponent<Utility_EffectsStack>();
        RightWordBrah += OptionSelectionManager.Instance.CorrectWordChosen;

        WrongWordBrah += _effects.CameraShakeSmall;
        WrongWordBrah += OptionSelectionManager.Instance.IncorrectWordChosen;
        _endCall += QuestionTimerManager.Instance.StartTime;
        _endCall += OptionSelectionManager.Instance.TurnOnDisplay;
        QuestionCounter.LowerTrigger += RoundEnded;


    }

    // Update is called once per frame
    void Update() {
        //if(Input.GetKeyUp(KeyCode.Space)) _endCall();


    }

    public void NewRound()
    {
        if (CurrentRound.RandomiseCategory) _previouslyUsed.Add(CurrentRound.RandomiseFocusCategory(_previouslyUsed)); //Randomise category

        QuestionCounter.ChangeScore(Rounds[_currentRound].WordsPerRound, true, true);
        _currentCategory = Rounds[_currentRound].RoundFocusCategory;
    }

    public void RoundEnded()
    {
        _currentRound++;
        _wordUnfoldTween.Kill();
        UrbanDictionaryGameManager.Instance.EndGame();

    }

    public void GuessMade()
    {
        if (_wordUnfoldTween != null && _wordUnfoldTween.IsPlaying()) StopWordUnfolding();
    }

    public void StopWordUnfolding()
    {
        _wordUnfoldTween.Kill();
        int curLength = WordDescription.text.Length;
        string newGoal = "\""+_currentWord.WordDescription.Substring(curLength - 1, _currentWord.WordDescription.Length - curLength)+ "\"";
        _wordUnfoldTween = UtilityFunctions.UnfoldTextOverTime(_textUnfoldTime / 5, WordDescription, "\"" + _currentWord.WordDescription+"\"", Ease.Linear, _endCall, WordDescription.text);
    }

    public void SelectNewWord(bool triggerTimer = false)
    {
        if (_trackedWords.Count > 0)
        {
            var pos = Random.Range(0, _trackedWords.Count);
            _currentWord = _trackedWords[pos];
            _trackedWords.RemoveAt(pos);

            UpdateUIWithWord(triggerTimer);
            OptionSelectionManager.Instance._controllingPlayer = PLAYER_IN_CONTROL.NONE;
        }
    }

    private void UpdateUIWithWord(bool triggerTime = false)
    {
        OptionSelectionManager.Instance.ChangeWordDisplays(false);
        _textUnfoldTime = UtilityFunctions.Map(_currentWord.WordDescription.Length, 0f, 300f, 1f, 25f);
        _wordUnfoldTween = UtilityFunctions.UnfoldTextOverTime(_textUnfoldTime, WordDescription, "\"" + _currentWord.WordDescription+"\"" , Ease.Linear, _endCall);
        QuestionTimerManager.Instance.EndQuestion();


        var textHold = "";
        string[] curAdded = new string[4];

        for (int i = 0; i < 4; i++)
        {
            textHold = UtilityFunctions.Choose<string>(_currentWord.SelectedWord, _currentWord.HerringWord1, _currentWord.HerringWord2, _currentWord.HerringWord3);
            while (UtilityFunctions.IsEqualToAny<string>(textHold, curAdded))
            {
                textHold = UtilityFunctions.Choose<string>(_currentWord.SelectedWord, _currentWord.HerringWord1, _currentWord.HerringWord2, _currentWord.HerringWord3);
            }
            WordPositions[i].text = textHold;
            if (_currentWord.SelectedWord == textHold) _correctWordPosition = i;

            curAdded[i] = textHold;

        }
        OptionSelectionManager.Instance.CorrectPosition = _correctWordPosition;
        QuestionCounter.ChangeScore(-1);
    }

    public void SetCategoryOptions(WORD_CATEGORY[] categoryChoices)
    {
        WordPositions[0].text = categoryChoices[0].ToString();
        WordPositions[1].text = categoryChoices[1].ToString();
        WordPositions[2].text = categoryChoices[2].ToString();
        WordPositions[3].text = categoryChoices[3].ToString();
        _categoryChoices = categoryChoices;
    }

    public void RefreshWordListForRound()
    {
        NewRound();
        _trackedWords.Clear();
        

        for(int i =0; i < Words.Count; i++)
        {
            var uw = Words[i];
            if (uw.WordCategory.Equals(_currentCategory) || _currentCategory == WORD_CATEGORY.ALL)
            {
                _trackedWords.Add(uw);
            }
        }
        
    }
    
    public void OptionSelected(int posInput, bool selectingCategories = false)
    {
        if(selectingCategories)
        {
            CurrentRound.RoundFocusCategory = _categoryChoices[posInput];
            UrbanDictionaryGameManager.Instance.NewGame();
            OptionSelectionManager.Instance.ChangeWordDisplays(false);
            return;
        }

        bool wasCorrect = false;
        OptionSelectionManager.Instance.AllowingInputs = false;
        if (WordPositions[posInput].text == _currentWord.SelectedWord)
        {            
            if(OptionSelectionManager.Instance._controllingPlayer == PLAYER_IN_CONTROL.ONE)
            {
                Player1Score.ChangeScore(Mathf.RoundToInt(CorrectScore * CurrentRound.PointsModifier));
            }
            else
            {
                Player2Score.ChangeScore(Mathf.RoundToInt(CorrectScore * CurrentRound.PointsModifier));
            }

            
            wasCorrect = true;
            if(RightWordBrah != null)RightWordBrah();
        }
        else //Incorrect
        {
            if (OptionSelectionManager.Instance._controllingPlayer == PLAYER_IN_CONTROL.ONE)
            {
                Player1Score.ChangeScore(IncorrectScore);
            }
            else
            {
                Player2Score.ChangeScore(IncorrectScore);
            }
            wasCorrect = false;
            WrongWordBrah();
        }

        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(WrongWordFeedbackTime);
        seq.AppendCallback(() =>
        {
            if(!wasCorrect) LivesManager.Instance.ChangeLives(-1);
            SelectNewWord();
        });
        //SelectNewWord();
        QuestionTimerManager.Instance.EndQuestion();

    }
}
