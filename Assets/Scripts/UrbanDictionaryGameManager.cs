﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Nyamakop;
using DG.Tweening;

public class UrbanDictionaryGameManager : Singleton<UrbanDictionaryGameManager> {

    [Header("End Game stuff")]
    public GameObject[] EndGameSwitchOff;
    public Text EndGameNotification, RestartGamePrompt;

    private bool _inEndState;

    private MyCharacterActions _charActions;

    public GAME_MODE CurrentMode = GAME_MODE.TWO;

    public ScoreMananger Score1, Score2, QuestionGuess;

    
	// Use this for initialization
	void Start () {
        LivesManager.Instance.LivesFinishedCall += EndGame;
        _charActions = new MyCharacterActions();
        _charActions.Initialize();
        NewGame();
    }


    public void Update()
    {
        if(_inEndState)
        {
            if(_charActions.Submit.WasReleased) //New game
            {
                NewGame();
            }
        }
    }

    public void NewGame()
    {
        Sequence se = DOTween.Sequence();
        
        se.AppendCallback(() =>
        {
            _inEndState = false;
            UtilityFunctions.SetActiveEnMasse(true, EndGameSwitchOff);
            EndGameNotification.text = "";
            RestartGamePrompt.text = "";

            UrbanDictionaryWordManager.Instance.RefreshWordListForRound();
            LivesManager.Instance.RegenerateLives();

            Score1.ResetScore();
            Score2.ResetScore();
            QuestionGuess.ResetScore();
        });
        se.AppendInterval(1f);
        se.AppendCallback(() => UrbanDictionaryWordManager.Instance.SelectNewWord(true));
        
    }

    public void EndGame()
    {
        UtilityFunctions.SetActiveEnMasse(false, EndGameSwitchOff);
        string winner = Score1.CurrentScore > Score2.CurrentScore ? "Player 1" : "Player 2";
        
        EndGameNotification.text = winner + " in the lead!";
        if (Score2.CurrentScore == Score1.CurrentScore) EndGameNotification.text = "You both suck (a tie)";

        var curRound = UrbanDictionaryWordManager.Instance.CurrentRound;
        RestartGamePrompt.text = "Press enter for next round:\n\n" + curRound.RoundPublicName+"\n " + curRound.PointsModifier+"x points!";
        _inEndState = true;

        if(UrbanDictionaryWordManager.Instance.CurrentRound.ChooseCategory)
        {
            OptionSelectionManager.Instance.SelectingCategories = true;
            OptionSelectionManager.Instance.ChangeWordDisplays(true);
            WORD_CATEGORY[] values = UtilityFunctions.ReturnEnumRandomValues<WORD_CATEGORY>(typeof(WORD_CATEGORY), 4, WORD_CATEGORY.ALL).ToArray();
            UrbanDictionaryWordManager.Instance.SetCategoryOptions(values);
            RestartGamePrompt.text = "Select a category for the next round!:\n\n" + curRound.RoundPublicName + "\n " + curRound.PointsModifier + "x points!";
        }
    }
}
