﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Nyamakop;

public class QuestionTimerManager : Singleton<QuestionTimerManager> {

    public float QuestionTime;
    private bool _isCountingDown;
    public Color StartColor, EndColor;

    private float _curTime;
    private float _curPerc;

    private Slider _slider;
    public Image Fill;

	// Use this for initialization
	void Start () {
        _curTime = QuestionTime;
        _slider = GetComponent<Slider>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(_isCountingDown)
        {
            if(_curTime <= 0)
            {
                _isCountingDown = false;
                EndQuestion(true);
            }
            else
            {
                _curTime -= Time.deltaTime;
                _curPerc = _curTime / QuestionTime;
            }
        }

        _slider.value = _curPerc;
        Fill.color = Color.Lerp(EndColor, StartColor, _curPerc);
	}

    public void EndQuestion(bool takeLife = false)
    {
        _isCountingDown = false;
        _curTime = QuestionTime;

        if (takeLife)
        {
            LivesManager.Instance.ChangeLives(-1);
            UrbanDictionaryWordManager.Instance.SelectNewWord(true);
        }
    }
    
    public void StartTime()
    {
        _curTime = QuestionTime;
        _isCountingDown = true;
    }
}
