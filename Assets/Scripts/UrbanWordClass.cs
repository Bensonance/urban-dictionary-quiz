﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UrbanWordClass {

    public string SelectedWord;
    public string WordDescription;
    public string HerringWord1, HerringWord2, HerringWord3;

    public WORD_CATEGORY WordCategory = WORD_CATEGORY.ALL;
    public string[] ReturnWords()
    {
        string[] wordOptions = new string[4];
        wordOptions[0] = SelectedWord;
        wordOptions[1] = HerringWord1;
        wordOptions[2] = HerringWord2;
        wordOptions[3] = HerringWord3;
        return wordOptions;
    }
}
