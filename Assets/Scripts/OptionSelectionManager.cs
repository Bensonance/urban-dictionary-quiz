﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Nyamakop;

public class OptionSelectionManager : Singleton<OptionSelectionManager> {

    private MyCharacterActions _inputControl, _inputControl2;

    public bool AllowingInputs = false;
    public Text[] WordOptions;
    public Text[] WordSelectionControls;
    public Utility_UI_EffectsStack[] WordOptionsEffects;
    public int CorrectPosition, ChosenPosition;
    // Use this for initialization
    public AudioClip RightSound, WrongSound;
    public Text PlayerIndicator;

    public PLAYER_IN_CONTROL _controllingPlayer = PLAYER_IN_CONTROL.NONE;
    public AudioClip PlayerSelect;
    public bool SelectingCategories;

    private bool _textUnfolding = true;

    public Color OptionDefaultColor, OptionFlashColor, OptionFlashColor2;
    

    void Start () {
        _inputControl = new MyCharacterActions();
        _inputControl.Initialize();
        //_inputControl.Device = InControl.InputManager.Devices[0];

        if (InControl.InputManager.Devices.Count >1 )
        {
            _inputControl2 = new MyCharacterActions();
            _inputControl2.Initialize();
            _inputControl2.Device = InControl.InputManager.Devices[1];
        }

        /*
        WordSelectionControls[0].text = _inputControl.Input1.Bindings[0].Name;
        WordSelectionControls[1].text = _inputControl.Input2.Bindings[0].Name;
        WordSelectionControls[2].text = _inputControl.Input3.Bindings[0].Name;
        WordSelectionControls[3].text = _inputControl.Input4.Bindings[0].Name;*/

        
    }

    public void TurnOnDisplay()
    {
        StartCoroutine(DelayInputs(0.5f));
        ChangeWordDisplays(true);
        _textUnfolding = false;
    }
	
    public void ChangeWordDisplays(bool display)
    {
        if(!display) _textUnfolding = true;
        if(!display)AllowingInputs = false;
        foreach (Text t in WordOptions)
        {
            var col = t.color;
            col.a = display ? 1 : 0;
            t.color = col;
        }

        foreach (Text t in WordSelectionControls)
        {
            var col = t.color;
            col.a = display ? 1 : 0;
            t.color = col;
        }
        
    }

	// Update is called once per frame
	void Update () {

        if (_inputControl != null && ((AllowingInputs || _textUnfolding) || SelectingCategories)) CheckForChoices(_inputControl);
        if(_inputControl2 != null && ((AllowingInputs || _textUnfolding)|| SelectingCategories)) CheckForChoices(_inputControl2);

        return;
        if (!AllowingInputs || _controllingPlayer == PLAYER_IN_CONTROL.NONE)
        {
            if (!PlayerIndicator.text.Equals("") && _controllingPlayer == PLAYER_IN_CONTROL.NONE)
            {
                PlayerIndicator.text = "";
                //PlayerIndicator.GetComponent<Utility_UI_EffectsStack>().ScalePulse(Vector3.one * 1.2f, 0.02f, 0.05f, DG.Tweening.Ease.Linear);
            }
        }
        else
        {
            if (_controllingPlayer == PLAYER_IN_CONTROL.ONE)
            {
                CheckForChoices(_inputControl);
            }
            else if (_controllingPlayer == PLAYER_IN_CONTROL.TWO)
            {
                CheckForChoices(_inputControl2);
            }
        }

        if (_controllingPlayer == PLAYER_IN_CONTROL.NONE)
        {
            if (_inputControl.Player1Default.WasReleased)
            {
                _controllingPlayer = PLAYER_IN_CONTROL.ONE;
                PlayerIndicator.GetComponent<Utility_UI_EffectsStack>().ScalePulse(Vector3.one * 1.5f, 0.02f, 0.05f, DG.Tweening.Ease.Linear);
                PlayerIndicator.text = "Player 1 guessing";
                PlayerIndicator.color = Color.yellow;
                SFX.PlayAt(PlayerSelect, transform.position, 1f, 0f, true);
                UrbanDictionaryWordManager.Instance.GuessMade();

            }
            else if (_inputControl2 != null && _inputControl2.Player1Default.WasReleased)
            {
                _controllingPlayer = PLAYER_IN_CONTROL.TWO;
                PlayerIndicator.GetComponent<Utility_UI_EffectsStack>().ScalePulse(Vector3.one * 1.5f, 0.02f, 0.05f, DG.Tweening.Ease.Linear);
                PlayerIndicator.text = "Player 2 guessing";
                PlayerIndicator.color = Color.cyan;
                SFX.PlayAt(PlayerSelect, transform.position, 1f, 0f, true);
                UrbanDictionaryWordManager.Instance.GuessMade();

            }
        }





    }

    public void CheckForChoices(MyCharacterActions checkedInput)
    {
        if (checkedInput.Input1.WasReleased)
        {
            ChosenOption(0);
        }
        else if (checkedInput.Input2.WasReleased)
        {
            ChosenOption(1);
        }
        else if (checkedInput.Input3.WasReleased)
        {
            ChosenOption(2);
        }
        else if (checkedInput.Input4.WasReleased)
        {
            ChosenOption(3);
        }
    }

    private void ChosenOption(int choice)
    {
        if (SelectingCategories)
        {
            UrbanDictionaryWordManager.Instance.OptionSelected(choice, true);
            SelectingCategories = false;
            return;
        }
        else if (_textUnfolding)
        {
            _textUnfolding = false;
            UrbanDictionaryWordManager.Instance.GuessMade();
        }
        else if(UrbanDictionaryWordManager.Instance.DescriptionFinishedUnfolding)
        {
            AllowingInputs = false;
            ChosenPosition = choice;
            UrbanDictionaryWordManager.Instance.OptionSelected(choice);
            WordOptionsEffects[choice].ScalePulse(Vector3.one * 1.5f, 0.07f, 0.1f);
        }
    }

    void WordChosenFeedback()
    {

    }

    public void CorrectWordChosen()
    {
        WordOptionsEffects[CorrectPosition].FlashColorChangeStart(UrbanDictionaryWordManager.WrongWordFeedbackTime, 0.3f, OptionFlashColor, OptionFlashColor2, OptionDefaultColor);
        SFX.PlayAt(RightSound, transform.position, 1f, Random.Range(0.9f, 1.1f), 0f);
    }

    public void IncorrectWordChosen()
    {
        WordOptionsEffects[ChosenPosition].FlashColorChangeStart(UrbanDictionaryWordManager.WrongWordFeedbackTime, 0.3f, Color.red, OptionFlashColor2, OptionDefaultColor);
        WordOptionsEffects[CorrectPosition].AngleOscillateStart(10f, 0.3f, UrbanDictionaryWordManager.WrongWordFeedbackTime);
        WordOptionsEffects[CorrectPosition].FlashColorChangeStart(UrbanDictionaryWordManager.WrongWordFeedbackTime, 0.3f, OptionFlashColor, OptionFlashColor2, OptionDefaultColor);
        SFX.PlayAt(WrongSound, transform.position, 1f, Random.Range(0.9f, 1.1f), 0f);
    }

    IEnumerator DelayInputs(float period)
    {
        yield return new WaitForSeconds(period);
        AllowingInputs = true;
    }
}
