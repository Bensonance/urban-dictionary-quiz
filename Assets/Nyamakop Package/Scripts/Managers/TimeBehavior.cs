﻿using UnityEngine;


public class TimeBehavior : Nyamakop.Singleton<TimeBehavior>
{

    private float _pauseTime;
    private float _returnSpeed;

    private bool paused;
    
    private void Update()
    {
        if (paused)
        {
            return;
        }

        if (_pauseTime <= 0)
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, 1f, _returnSpeed * Time.unscaledDeltaTime);
        }
        else
        {
            _pauseTime -= Time.unscaledDeltaTime;
        }
    }

    public void PauseGame(float pauseTime, float timeScale)
    {
        _pauseTime = pauseTime;
        Time.timeScale = timeScale;
    }

    public void AffectTime(float maintainTime, float returnSpeed, float newTimeScale)
    {
        _pauseTime = maintainTime;
        _returnSpeed = returnSpeed;
        Time.timeScale = newTimeScale;
    }

    public void Start()
    {
        //GameManager.Instance.OnPause += OnPause;
    }


    private void OnDestroy()
    {
        
    }

    private void OnPause(bool pause)
    {
        this.paused = pause;
    }
}
