﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nyamakop;
using UnityEngine.UI;

public class LivesManager : Singleton<LivesManager> {

    public int CurrentLives = 3;
    private int _startLives;
    public GameObject LifeUIInstance;
    private List<GameObject> _lives = new List<GameObject>();

    public float LifeXWidthDifference;

    //Delegate for piggbacking end of lives calls
    public delegate void LivesDepleted();
    public LivesDepleted LivesFinishedCall;

    public bool Display;

	// Use this for initialization
	void Start () {
        _startLives = CurrentLives;
        GenerateLives();
	}
	
    public void ChangeLives(int change)
    {
        if (!Display) return;
        if(CurrentLives + change < CurrentLives)//Deducted
        {
            var pos = _lives.Count - 1;
            Destroy(_lives[pos]);
            _lives.RemoveAt(pos);
        }else if(CurrentLives + change > CurrentLives)//Added
        {
            //To be added (needs to get possition of last life in array and move from there
        }
        CurrentLives+= change;
        if (CurrentLives <= 0) LivesFinishedCall();
    }

    void GenerateLives()
    {
        if (!Display) return;

        var rectTrans = GetComponent<RectTransform>();
        Vector3 startPos = rectTrans.position - new Vector3(rectTrans.sizeDelta.x/2, 0f);
        for(int i = 0; i < CurrentLives; i++)
        {
            CreateLife(startPos, new Vector3(i * LifeXWidthDifference * rectTrans.sizeDelta.x, 0f));
        }
    }

    void CreateLife(Vector3 startPos, Vector3 posOffset)
    {
        GameObject newLife = (GameObject)(Instantiate(LifeUIInstance, startPos, Quaternion.identity));
        newLife.transform.parent = this.gameObject.transform;
        newLife.transform.position += posOffset;
        _lives.Add(newLife);
    }

    public void RegenerateLives()
    {
        for(int i = 0; i < _lives.Count; i++)
        {
            Destroy(_lives[i]);
        }
        _lives.Clear();
        CurrentLives = _startLives;
        GenerateLives();
    }
}
