﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    private MyCharacterActions _playerActions;
    public int SceneLoaded;
	// Use this for initialization
	void Start () {
        _playerActions = new MyCharacterActions();
        _playerActions.Initialize();
	}
	
	// Update is called once per frame
	void Update () {
		if(_playerActions.Input1.WasReleased)
        {
            SceneManager.LoadScene(SceneLoaded, LoadSceneMode.Single);
        }
	}
}
