﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeController : MonoBehaviour {

    public float Life =100;
    public bool AutoDie = true;
    public bool AllModificationNegative = true;

    public delegate void DamagedDelegate();
    public DamagedDelegate InstanceDamaged;

    public delegate void HealedDelegate();
    public HealedDelegate InstanceHealed;

    public delegate void DeathDelegate();
    public DeathDelegate InstanceDied;

    [Header("Damage Cooldown")]
    public float TimeUntilNextDamage =1f;
    public bool CanBeDamaged, IsDead;
    private float _currentDamageTickdown;

    public void LateUpdate()
    {
        if (!CanBeDamaged)
        {
            if (_currentDamageTickdown <= 0)
            {
                CanBeDamaged = true;
            }
            else
            {
                _currentDamageTickdown -= Time.deltaTime;
            }
        }
        if (AutoDie && Life <= 0) Die();
    }

    public void ModifyLife(float amount)
    {
        //If a damage modification and damage cooldown still in effect, return
        if ((amount <0 || AllModificationNegative)&& !CanBeDamaged) return;

        if (AllModificationNegative && Mathf.Sign(amount) > 0) amount *= -1;
        Life += amount;
        Life = Mathf.Clamp(Life, 0, 100);

        if (Mathf.Sign(amount) < 0 && InstanceDamaged != null) InstanceDamaged(); //Allow for backpacking calls when instance damaged
        if (Mathf.Sign(amount) > 0 && InstanceHealed != null) InstanceHealed();

        if (amount < 0)
        {
            CanBeDamaged = false;
            _currentDamageTickdown = TimeUntilNextDamage;
        }
    }

    public void Die()
    {
        //Destroy(this.gameObject);
        IsDead = true;
        if (InstanceDied != null) InstanceDied();
    }

    
}
