﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(LifeController))]
[RequireComponent(typeof(Collider2D))]
public class LifeCollisionControl : MonoBehaviour {

    private LifeController _life;
    public string[] CollisionNames;
    protected Utility_EffectsStack _effects;
    protected delegate void DamageTrigger();
    DamageTrigger DamageEffects;
    // Use this for initialization
    protected virtual void Awake() {
        _life = GetComponent<LifeController>();
        if (GetComponent<Utility_EffectsStack>() != null)
        {
            _effects = GetComponent<Utility_EffectsStack>();
            DamageEffects += _effects.DamageColorFlash;
        }
	}

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        var lifeModifier = collision.GetComponent<LifeModifier>();
        if (UtilityFunctions.IsEqualToAnyString(collision.tag, CollisionNames) && lifeModifier != null)
        {
            _life.ModifyLife(lifeModifier.SendLifeModification());
            if(DamageEffects != null)DamageEffects();
        }
    }

    private void Reset()
    {
        GetComponent<Collider2D>().isTrigger = true;
    }

}
