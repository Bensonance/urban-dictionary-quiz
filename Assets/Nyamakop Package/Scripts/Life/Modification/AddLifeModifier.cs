﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddLifeModifier : LifeModifier {

    public float LifeAdded;
    public override float SendLifeModification()
    {
        return LifeAdded;
    }
}
