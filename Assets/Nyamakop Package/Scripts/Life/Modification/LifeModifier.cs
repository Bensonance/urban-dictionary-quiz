﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class LifeModifier : MonoBehaviour {

    public bool DestroyOnModify;
    public virtual float SendLifeModification()
    {
        if(DestroyOnModify) Destroy(this.gameObject, 0.1f);
        return 0;
    }

    protected virtual void Reset()
    {
        GetComponent<Collider2D>().isTrigger = true;
    }
}
