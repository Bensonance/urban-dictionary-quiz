﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveLifeModifier : LifeModifier {

    public float LifeRemoved;
    public override float SendLifeModification()
    {
        base.SendLifeModification();
        return -LifeRemoved;
    }


}
