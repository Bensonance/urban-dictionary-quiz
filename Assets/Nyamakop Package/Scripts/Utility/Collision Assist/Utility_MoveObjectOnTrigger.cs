﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility_MoveObjectOnTrigger : MonoBehaviour {

    public string CollisionTag;
    public GameObject ObjectToMove;
    public Transform NewPosition;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.tag == CollisionTag)
        {
            ObjectToMove.transform.position = NewPosition.position;
            Destroy(gameObject);
        }
    }
}
