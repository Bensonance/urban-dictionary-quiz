﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

[RequireComponent(typeof(BoxCollider2D))]
public class EventColliderTriggerUtility : MonoBehaviour {

    public UnityEvent TriggeredEvent;
    public string TagChecked;
    public bool DisableAfterTrigger;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(TagChecked.Equals("") || collision.CompareTag(TagChecked))
        {
            TriggeredEvent.Invoke();
            if (DisableAfterTrigger) GetComponent<BoxCollider2D>().enabled = false;
        }
    }

}
