﻿using UnityEngine;
using System.Collections;

public class UtilityBobAttributes : MonoBehaviour
{

    [Header("Position")]
    public bool BobPosition, BobLocalPosition, BobDirection;
    public Vector3 OffsetPostion, DirectionBob;
    public Vector2 YFromTo;
    public Vector2 XFromTo;
    public Vector2 DirectionBobLength;
    private Vector2 _startYFromTo, _startXFromTo;
    public float YDuration, XDuration, DirectionBobDuration;

    public enum BobDirectionPreset{LEFT, RIGHT, DOWN, UP, MANUAL};
    public BobDirectionPreset ChosenDirectionPreset = BobDirectionPreset.MANUAL;

    [Header("Scale")]
    public bool BobScale, BobScaleAxisIndependently;
    private Vector3 _startScale;

    public float Offset;
    private float ScaleOffset;
    private bool _randomiseOffset = true;
    public bool RandomChance;
    private Renderer _render;

    public Vector2 ScaleBobRange;
    public float ScaleBobDuration;

    public float ScaleBobDurationX, ScaleBobDurationY;

    [Header("Angle")]
    public bool BobAngle, BobGlobalAngle;
    private Vector3 _startAngle;
    public float AngleAmount;
    public float AngleTime;

    [Header("Miscellaneous")]
    public bool UnParentOnStart;
    
    private float lifeTime = 0;
    

    // Use this for initialization
    void Awake()
    {
        if(ChosenDirectionPreset != BobDirectionPreset.MANUAL)
        {
            switch (ChosenDirectionPreset)
            {
                case BobDirectionPreset.RIGHT:
                    DirectionBob = transform.right;
                    break;

                case BobDirectionPreset.LEFT:
                    DirectionBob = -transform.right;
                    break;

                case BobDirectionPreset.UP:
                    DirectionBob = transform.up;
                    break;

                case BobDirectionPreset.DOWN:
                    DirectionBob = -transform.up;
                    break;
            }

        }
        OffsetPostion = transform.position;
        _startScale = transform.localScale;
        _startYFromTo = YFromTo;
        _render = GetComponent<Renderer>();
        RandomChance = UtilityFunctions.Chance(100);
        
        if (_randomiseOffset)
        {
            Offset = Random.Range(0f, 1f);
            ScaleOffset = Random.Range(0f, 1f);
        }

        if(_render != null)GameObject.Destroy(_render.GetComponent<Collider>());
        _startAngle = transform.localEulerAngles;
        //YFromTo.x = _startPostion.y + YFromTo.x;
        //YFromTo.y = _startPostion.y - YFromTo.y;
        
        if (UnParentOnStart) transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (BobPosition && (XFromTo != Vector2.zero || YFromTo != Vector2.zero))
            BobWavePosition();

        //#if !UNITY_EDITOR
        if (BobScale && RandomChance && _render.isVisible)
        {
            BobWaveScale();
        }
        //#endif

        if (BobAngle)
        {
            BobWaveAngle();
        }
        
        lifeTime += Time.deltaTime;
    }

    private void BobWavePosition()
    {
        if(BobDirection)
        {
            float intensity = UtilityFunctions.BobWave(DirectionBobLength.x, DirectionBobLength.y, DirectionBobDuration, Offset, lifeTime);
            transform.position = OffsetPostion + DirectionBob.normalized * intensity;
        }
        else
        if (BobLocalPosition)
        {
            transform.localPosition = new Vector3(
                UtilityFunctions.BobWave(XFromTo.x, XFromTo.y, XDuration, Offset, lifeTime),
                UtilityFunctions.BobWave(YFromTo.x, YFromTo.y, YDuration, Offset, lifeTime),
                0f
            );
        }
        else {
            transform.position = OffsetPostion + new Vector3(
                UtilityFunctions.BobWave(XFromTo.x, XFromTo.y, XDuration, Offset, lifeTime),
                UtilityFunctions.BobWave(YFromTo.x, YFromTo.y, YDuration, Offset, lifeTime),
                0f
            );
        }
    }

    public void StartBobWaveAngle()
    {
        BobAngle = true;
        float jumpAmount = _startAngle.z + UtilityFunctions.BobWave(-AngleAmount, AngleAmount, AngleTime, Offset, lifeTime);
        float testOffset = 0f;
        while(jumpAmount != transform.eulerAngles.z && testOffset <1f)
        {
            jumpAmount = _startAngle.z + UtilityFunctions.BobWave(-AngleAmount, AngleAmount, AngleTime, testOffset, lifeTime);
            testOffset += 0.01f;
        }
        Offset = testOffset;
    }

    private void BobWaveAngle()
    {
        if (BobGlobalAngle)
        {
            transform.eulerAngles = new Vector3(_startAngle.x, _startAngle.y, _startAngle.z + UtilityFunctions.BobWave(-AngleAmount, AngleAmount, AngleTime, Offset, lifeTime)); 
        }
        else {
            transform.localEulerAngles = new Vector3(_startAngle.x, _startAngle.y, _startAngle.z + UtilityFunctions.BobWave(-AngleAmount, AngleAmount, AngleTime, Offset, lifeTime));
        }
        
    }

    private void BobWaveScale()
    {

        if (BobScaleAxisIndependently)
        {
            var scaleX = UtilityFunctions.BobWave(ScaleBobRange.x, ScaleBobRange.y, ScaleBobDurationX, ScaleOffset, lifeTime);
            var scaleY = UtilityFunctions.BobWave(ScaleBobRange.x, ScaleBobRange.y, ScaleBobDurationY, ScaleOffset, lifeTime);
            transform.localScale = _startScale + new Vector3(scaleX, scaleY, 0f);
        }
        else
        {
            var scale = UtilityFunctions.BobWave(ScaleBobRange.x, ScaleBobRange.y, ScaleBobDuration, ScaleOffset, lifeTime);
            transform.localScale = _startScale + new Vector3(scale, scale, 0f);
        }
    }

    public void LoadNewAttributes(Vector2 scaleBobRange, float xDuration, float yDuration)
    {
        ScaleBobRange = scaleBobRange;
        ScaleBobDurationX = xDuration;
        ScaleBobDurationY = yDuration;
        BobScaleAxisIndependently = true;
    }
}
