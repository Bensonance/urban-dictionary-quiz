﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class DisableSpriteOnStart : MonoBehaviour
{
    void Start()
    {
		GetComponent<SpriteRenderer>().enabled = false;
    }
}
