﻿using UnityEngine;
using System.Collections;

public class DestroyOnStart : MonoBehaviour {

    public float DestroyDelay;
	// Use this for initialization
	void Start () {
		Destroy(gameObject, DestroyDelay);
	}
}
