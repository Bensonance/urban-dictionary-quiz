﻿using UnityEngine;
using System.Collections;

public class UtilityParticleEruptDelay : MonoBehaviour {
    public float DelayTime;
    private ParticleSystem _particles;

	// Use this for initialization
	void Start () {
        StartCoroutine(DelayParticleBurst(DelayTime));
        _particles = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator DelayParticleBurst(float period)
    {
        yield return new WaitForSeconds(period);
        _particles.Play();
    }
}
