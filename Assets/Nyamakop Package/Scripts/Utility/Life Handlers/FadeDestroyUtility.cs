﻿using UnityEngine;
using System.Collections;

public class FadeDestroyUtility : MonoBehaviour {

	public float FadeTime;
	private float _curTime;
	private Renderer _render;
	private float _alphaOffsetPerc;
	public Color ColorTint;

	private LineRenderer _line;

	public bool FadeFastAtEnd;

	public bool SetChildren;
	// Use this for initialization
	void Start () {
		_curTime = FadeTime;
		_render = GetComponent<Renderer> ();
		_line = GetComponent<LineRenderer> ();
		Color curColor = ColorTint;
		curColor.a = 1f - _alphaOffsetPerc;
		ColorTint = curColor;
		_render.material.color = curColor;
	}

	public void SetupTime(float time)
	{
		FadeTime = time;
		_curTime = time;
	}

	public void SetupAlphaOffset(float alpha, Color col)
	{
		ColorTint = col;
		_render = GetComponent<Renderer> ();
		_alphaOffsetPerc = alpha;
		Color curColor = ColorTint;
		curColor.a = 1f - _alphaOffsetPerc;
		_render.material.color = curColor;
		if(_line != null)
			_line.SetColors(curColor, curColor);
	}
	
	// Update is called once per frame
	void Update () {
		if (!FadeFastAtEnd) {
			Color curColor = ColorTint;
			float curPerc = _curTime / FadeTime;
			curColor.a = Mathf.Lerp (0f, 1f - _alphaOffsetPerc, curPerc);
			ColorTint = curColor;
			_render.material.color = curColor;
			if(_line != null)
				_line.SetColors(curColor, curColor);
			if(SetChildren){
				Renderer[] _rendersChild = GetComponentsInChildren<Renderer>();
				foreach(Renderer r in _rendersChild){
					r.material.color = curColor;
				}
			}
		}

		if(_curTime <= 0){
			if(FadeFastAtEnd){
				Color curColor = ColorTint;
				curColor.a = Mathf.Lerp (curColor.a, 0f, 0.1f);
				ColorTint = curColor;
				_render.material.color = curColor;
				if(_line != null)
					_line.SetColors(curColor, curColor);

				if(SetChildren){
					Renderer[] _rendersChild = GetComponentsInChildren<Renderer>();
					foreach(Renderer r in _rendersChild){
						r.material.color = curColor;
					}
				}
				if(curColor.a < 0.0025)Destroy(gameObject);
			}else{
				Destroy(gameObject);
			}
		}else{
			_curTime -= Time.deltaTime;
		}
	}
}
