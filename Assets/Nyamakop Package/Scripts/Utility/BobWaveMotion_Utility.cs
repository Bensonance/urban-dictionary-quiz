﻿using UnityEngine;
using System.Collections;

public class BobWaveMotion_Utility : MonoBehaviour {
    private Vector3 _startPosition;

    public Vector2 XBobRange;
    public float XDuration;
    public Vector2 YBobRange;
    public float YDuration;

    public bool MapToBob;
    public Vector2 RotationRange;
    private float _XOffset;
    // Use this for initialization
    void Start () {
        _startPosition = transform.localPosition;
        _XOffset = Random.Range(0f, 1f);

    }
	
	// Update is called once per frame
	void Update () {
        float xPos = UtilityFunctions.BobWave(XBobRange.x, XBobRange.y, XDuration, _XOffset);
        float yPos = UtilityFunctions.BobWave(YBobRange.x, YBobRange.y, YDuration, 0f);
        if(MapToBob)transform.localEulerAngles = new Vector3(0f, 0f, UtilityFunctions.Map(xPos, XBobRange.x, XBobRange.y, RotationRange.x, RotationRange.y));
        transform.localPosition = _startPosition + new Vector3(xPos, yPos, 0f);
    }
}
