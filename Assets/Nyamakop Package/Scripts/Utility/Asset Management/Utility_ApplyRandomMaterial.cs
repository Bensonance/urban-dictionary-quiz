﻿using UnityEngine;
using System.Collections;

public class Utility_ApplyRandomMaterial : MonoBehaviour {

    public Material[] ParticleMats;
    public Material[] ParticleMats2;
    public bool Alts = false;

	// Use this for initialization
	void Start () {
        if(!Alts)
        {
            GetComponent<ParticleSystemRenderer>().material = ParticleMats[Mathf.RoundToInt(Random.Range(0, ParticleMats.Length))];
        }
        else if(transform.parent.name.Contains("Horizontal"))
        {
            GetComponent<ParticleSystemRenderer>().material = ParticleMats2[Mathf.RoundToInt(Random.Range(0, ParticleMats.Length))];
        }
        else
        {
            GetComponent<ParticleSystemRenderer>().material = ParticleMats[Mathf.RoundToInt(Random.Range(0, ParticleMats.Length))];
        }
        
	}
	
}
