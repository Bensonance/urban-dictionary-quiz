﻿using UnityEngine;
using System.Collections;

public class UtilitySendTriggerMessage : MonoBehaviour {

    public bool OnEnter, OnStay, OnEnd;
    public string[] TriggerTags;
    public string Message;
    public GameObject MessageTarget;
    public bool MessageToParent;
    
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (!OnEnter)
            return;
        for(int i = 0; i < TriggerTags.Length; i++)
        {
            if(coll.tag.Equals(TriggerTags[i]))
            {
                if(MessageToParent)
                {
                    transform.parent.SendMessage(Message);
                }
            }
        }
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        if (!OnStay)
            return;

        for (int i = 0; i < TriggerTags.Length; i++)
        {
            if (coll.tag.Equals(TriggerTags[i]))
            {
                if (MessageToParent)
                {
                    transform.parent.SendMessage(Message);
                    Debug.Log("Sernding");
                }
            }
        }
    }
}
