﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

//[ExecuteInEditMode]
public class Utility_ApplyRandomSprite : MonoBehaviour
{

    public Sprite[] SpriteOptions;
    public bool SetSeed;
    public int RandomSeed;
    public bool GenerateSpriteOnStart;
    public bool GrowPlantActual = false;
    public AudioClip[] FoompSounds;
    // Use this for initialization

    void Awake()
    {
        RandomSeed = (int)(transform.position.x + transform.position.y / 2);
    }
    void Start()
    {
        if (GenerateSpriteOnStart)
            GenerateNewSprite();


        if (GrowPlantActual)
            GrowPlant();
    }

    public void GenerateNewSprite()
    {
        if (Application.isPlaying && RandomSeed == 0)
        {
            //RandomSeed = Random.Range(-200, 200);
        }

        //Random.seed = RandomSeed;
        gameObject.GetComponent<SpriteRenderer>().sprite = SpriteOptions[Mathf.RoundToInt(Random.Range(0, SpriteOptions.Length))];

        if (GetComponent<PolygonCollider2D>() != null)
            Destroy(GetComponent<PolygonCollider2D>());

        if (Application.isPlaying)
        {
            gameObject.AddComponent<PolygonCollider2D>();
            gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
        }

        //Random.seed = Random.Range(-200, 200);
    }

    public void GrowPlant(Transform parentAfterTween = null)
    {
        float time = Random.Range(0.5f, 2f);
        transform.localScale = Vector3.zero;

        transform.DOScale(new Vector3(0f, 1f), time).SetEase(Ease.InOutElastic).OnComplete(() => SetTransformParent(parentAfterTween));
    }

    public void SetTransformParent(Transform t)
    {
        transform.parent = t;
    }
}
