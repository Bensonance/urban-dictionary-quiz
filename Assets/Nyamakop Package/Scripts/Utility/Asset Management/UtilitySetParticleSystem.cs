﻿using UnityEngine;
using System.Collections;

public class UtilitySetParticleSystem : MonoBehaviour
{
	
    public string layerString;
    public int layerOrder;
    
    void Start()
    {
        GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = layerString;
        GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder = layerOrder;
    }
}
