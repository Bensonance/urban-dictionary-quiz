﻿using UnityEngine;
using System.Collections;

public class SetRendererLayer : MonoBehaviour
{
	public SpriteProperties Props;

    void Start()
    {
	
    }
	
    void Update()
    {
	
    }

    void OnDrawGizmos()
    {
    	var r = GetComponent<Renderer>();
    	// var mat = new Material(r.sharedMaterial.shader);
    	r.sortingOrder = Props.SortingLayerOrder;
    	r.sortingLayerName = Props.SortingLayerName;
    }
}
