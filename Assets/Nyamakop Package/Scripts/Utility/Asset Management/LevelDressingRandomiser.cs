﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LevelDressingRandomiser : MonoBehaviour {
    public bool SetSeed;
    public int RandomSeed;
    public float SeedVariance;
    public bool AffectRotation;
    public Vector2 RotationRange;

    public bool ApplyRandomSprite;
    public Sprite[] SpriteOptions;

    public bool ChangeCollider;
    public bool RandomiseLayer;
    public bool SetLayerOnCollision;
    public bool Darker;
    // Use this for initialization

    void Awake()
    {
        if (RandomSeed == 0) RandomSeed = (int)Random.Range(-200, 200);
    }

    void Start () {
        //GetComponent<SpriteRenderer>().flipX = UtilityFunctions.Chance(50);
        if (GetComponent<PolygonCollider2D>() != null) Destroy(GetComponent<PolygonCollider2D>());
        RandomiseDressing();
	}
	
    private void RandomiseDressing()
    {
        if(SetSeed)Random.seed = RandomSeed;
        if(AffectRotation)
        {
            Vector3 curRot = transform.localEulerAngles;
            curRot.z = Random.Range(RotationRange.x, RotationRange.y);
            transform.localEulerAngles = curRot;
        }

        

        if (ApplyRandomSprite)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SpriteOptions[Mathf.RoundToInt(Random.Range(0, SpriteOptions.Length))];
        }

        if(RandomiseLayer)
        {
            bool chance = UtilityFunctions.Chance(25f);
            GetComponent<SpriteRenderer>().sortingLayerName = chance ? "Foreground" : "Middleground";
            GetComponent<SpriteRenderer>().sortingOrder = chance ? 4 : -2;
            UpdateSpriteValue();
        }

        if (Darker) UpdateSpriteValue();
        

        if(ChangeCollider && Application.isPlaying)
        {
            gameObject.AddComponent<PolygonCollider2D>();
            gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
        }

        if (SetSeed) Random.seed = (int)Random.Range(-200, 200);
    }
    

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (SetLayerOnCollision && coll.name.Contains("Tree"))
        {
            GetComponent<SpriteRenderer>().sortingLayerName = "Foreground";
            GetComponent<SpriteRenderer>().sortingOrder = 4;
            UpdateSpriteValue();

            coll.GetComponent<SpriteRenderer>().sortingLayerName = "Background";
            coll.GetComponent<SpriteRenderer>().sortingOrder = -2;
            coll.GetComponent<LevelDressingRandomiser>().UpdateSpriteValue();
        } 
    }

    private void UpdateSpriteValue()
    {
        Color col = GetComponent<SpriteRenderer>().color;
        float h;
        float s;
        float v;
        Color.RGBToHSV(col, out h, out s, out v);
        v = GetComponent<SpriteRenderer>().sortingOrder <0 || Darker ? 180f / 255f : 220f / 255f;
        col = Color.HSVToRGB(h, s, v);
        GetComponent<SpriteRenderer>().color = col;
    }
}
