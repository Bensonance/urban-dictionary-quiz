﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//[RequireComponent(typeof(Renderer))]
public class AnimationManualUtility : MonoBehaviour {

	public Sprite[] sprites;
	public float FPS =24;
	private float _curFrameTime;
	private float _timeTillNextFrame;
	private int _curPos;
    public int CurrentFrame
    {
        get
        {
            return _curPos;
        }

        set
        {
            _curPos = value;
        }
    }
    private int _animDirection = 1;

	private SpriteRenderer _render;

	public bool DestroyInPeriod;
	public float DestroyPeriod;
	public bool DestroyAtEnd;
	public bool AnimateOverPeriod;
	public float AnimationPeriod;
    public bool PlayOnAwake = true;

	[HideInInspector]
	public string MessageSent;
	[HideInInspector]
	public GameObject MessageReciever;
	public bool SendMessageEndAnimation;//If false, sends message on destroy
	public int FramesBeforeEndToSend;

	public bool PlaySoundOnStart;

	private float _curTimePeriod;

	public bool DelayStart;
	public float DelayStartTime;
	private float _curTime;
	private bool _canAnimate;

    [Header("Animation Types")]
    public bool PingPong;
    public bool TimedToOtherAnimation;
    public AnimationManualUtility TimingTarget;
    private bool _isInTime = false;

    [Header("Miscellaneous")]
    public bool UIAnimation;
    private Image _renderImage;
	// Use this for initialization
	void Start () {
		_curTime = DelayStartTime;
        SetTimeToNextFrame();
        _curFrameTime = _timeTillNextFrame;
		_render = GetComponent<SpriteRenderer> ();
        if (UIAnimation) _renderImage = GetComponent<Image>();
         _curTimePeriod = DestroyPeriod;
		if(PlayOnAwake) _canAnimate = !DelayStart;
	}
	
	// Update is called once per frame
	void Update () {
        SetTimeToNextFrame();
        if (_canAnimate)Animate ();
		if(PlayOnAwake)DelayStartCountdown ();
		Countdown ();
	}

    public void UpdateSprites(Sprite[] newSprites)
    {
        sprites = newSprites;
        SetTimeToNextFrame();
        _curFrameTime = _timeTillNextFrame;

        // Debug.Log("Period: " + AnimationPeriod);
        // Debug.Log("Array: " + sprites.Length);
        // Debug.Log("Name: " + gameObject.name);
    }

	private void DelayStartCountdown()
	{
		if (_curTime <= 0) {
			_canAnimate = true;
		} else {
			_curTime -= Time.deltaTime;
		}
	}

    public void RealignTiming()
    {
        if (!TimedToOtherAnimation) return;
        _canAnimate = !DelayStart;
        _curTime = DelayStartTime;
        _curPos = 0;
        TimingTarget.RestartAnimation();
        SetFrame(_curPos);
    }

	private void Animate()
	{
        /*if (TimedToOtherAnimation && !_isInTime)
        {
            if (CurrentFrame != TimingTarget.CurrentFrame)
            {
                return;
            }else
            {
                _isInTime = true;
            }
        }*/
        
		if(_curFrameTime <= 0)
		{
			if(_curPos >= sprites.Length-1-FramesBeforeEndToSend){//Send message to object
				if(SendMessageEndAnimation && MessageSent != "")
				{
					MessageReciever.SendMessage(MessageSent);
					MessageSent = "";
				}
			}

            //If at end or beginning of animation
			if( (_curPos >= sprites.Length-1 && _animDirection == 1) || (_curPos <= 0 && _animDirection == -1))
			{
				if(DestroyAtEnd)
					Destroy(gameObject);

                if(PingPong)
                {
                    _animDirection *= -1;
                    _curPos += _animDirection;
                }
                else
                {
                    _curPos = 0;
                }
                
			}else{
				_curPos+= _animDirection;
			}
			_curFrameTime = _timeTillNextFrame;
		}else{
            _curFrameTime -= Time.deltaTime;
		}

        if (sprites.Length <= 0)
        {
            if (_render != null) _render.sprite = null;
            if (UIAnimation && _renderImage.sprite != null) _renderImage.sprite = null;
            return;
        }
        SetFrame(_curPos);
	}

    private void SetTimeToNextFrame()
    {
        _timeTillNextFrame = AnimateOverPeriod ? AnimationPeriod / sprites.Length : 1 / FPS;
    }

    private void SetFrame(int framePos)
    {
        if (framePos < 0 || framePos >= sprites.Length)
            return;

        if(_render != null)_render.sprite = sprites[framePos];
        if (UIAnimation) _renderImage.sprite = sprites[framePos];
    }

	private void Countdown()
	{
		if(DestroyInPeriod)
		{
			if(_curTimePeriod <= 0){
				Destroy(gameObject);
			}else{
				_curTimePeriod -= Time.deltaTime;
			}
		}
	}

	public void SetupMessage(string messageToSend, GameObject objectToSendTo, bool sendMessageEndOfAnim, int framesBeforeEnd)
	{
		MessageSent = messageToSend;
		MessageReciever = objectToSendTo;
		SendMessageEndAnimation = sendMessageEndOfAnim;
		FramesBeforeEndToSend = framesBeforeEnd;
	}

	void OnDestroy()
	{
		if(!SendMessageEndAnimation && MessageSent != "")
		{
			MessageReciever.SendMessage(MessageSent);
		}
	}

    //Frame Management
    public void NextFrame(bool forward)
    {
        if(forward && _curPos+1 < sprites.Length)
        {
            _curPos++;
        }
        else if(_curPos -1 > 0)
        {
            _curPos--;
        }
        if (_render != null) _render.sprite = sprites[_curPos];
        if (UIAnimation) _renderImage.sprite = sprites[_curPos];
    }

    public void RestartAnimation()
    {
        _curPos = 0;
        SetTimeToNextFrame();
        _curFrameTime = _timeTillNextFrame;
    }
}
