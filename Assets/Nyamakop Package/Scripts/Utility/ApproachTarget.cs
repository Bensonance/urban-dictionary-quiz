﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApproachTarget : MonoBehaviour {

    public GameObject Target;
    private Vector3 _prevPos, _prevTarget, _currentTarget;
    private ObliqueFaceMouse _facer;

    public bool AutoStart = true;
    public bool HasArrivedAtTarget;
    private bool _hasStarted;
    
    public float ApproachSpeed;

    public delegate void HasArrived();

    public HasArrived ArrivalDelegate;
	// Use this for initialization
	void Awake() {
        _hasStarted = AutoStart;
        ArrivalDelegate += Arrive;
    }
	
	// Update is called once per frame
	void Update () {
        if (!_hasStarted) return;
        _prevPos = transform.position;
        
        _currentTarget = Target.transform.position;
        transform.position = UtilityFunctions.SmoothApproach(_prevPos, _prevTarget, _currentTarget, ApproachSpeed);
        if (Vector3.Distance(_currentTarget, transform.position) < 0.1f && ArrivalDelegate != null) ArrivalDelegate();
        
    }

    private void LateUpdate()
    {
        _prevTarget = Target.transform.position;
    }

    public void StartApproach()
    {
        _hasStarted = true;
    }

    public void Arrive()
    {
        HasArrivedAtTarget = true;
    }
}
