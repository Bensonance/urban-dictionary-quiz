﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextMeshManageLength : MonoBehaviour {

    public int LineCharacterLength, LineNumber;
    private TextMesh _text;

	// Use this for initialization
	void Awake () {
        _text = GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
        _text.text = BreakPartIfNeeded(_text.text);
	}

    // Wrap text by line height
    string BreakPartIfNeeded(string part)
    {
        string saveText = _text.text;
        _text.text = part;

        if (_text.GetComponent<Renderer>().bounds.extents.x > LineCharacterLength)
        {
            string remaining = part;
            part = "";
            while (true)
            {
                int len;
                for (len = 2; len <= remaining.Length; len++)
                {
                    _text.text = remaining.Substring(0, len);
                    if (_text.GetComponent<Renderer>().bounds.extents.x > LineCharacterLength)
                    {
                        len--;
                        break;
                    }
                }
                if (len >= remaining.Length)
                {
                    part += remaining;
                    break;
                }
                part += remaining.Substring(0, len) + System.Environment.NewLine;
                remaining = remaining.Substring(len);
            }

            part = part.TrimEnd();
        }

        _text.text = saveText;

        return part;
    }
}
