﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

    public static class UtilityFunctions
    {
        

        public static float Map(float value, float initialMin, float initialMax, float destinationMin, float destinationMax)
        {
            var t = (value - initialMin) / (initialMax - initialMin);
            return Mathf.Lerp(destinationMin, destinationMax, t);
        }

        public static Vector2 RotateVector(float angle, Vector2 v)
        {
            var x = v.x * Mathf.Cos(angle * Mathf.Deg2Rad) - v.y * Mathf.Sin(angle * Mathf.Deg2Rad);
            var y = v.x * Mathf.Sin(angle * Mathf.Deg2Rad) + v.y * Mathf.Cos(angle * Mathf.Deg2Rad);

            return new Vector2(x, y);
        }

        public static bool Chance(float chance)
        {
            return Random.Range(0, 100) <= chance;
        }

        public static float RandomSign(float chanceForPositive)
        {
            return (Chance(chanceForPositive) ? 1 : -1);
        }

        public static Vector2 RandomPosition(Vector2 pos1, Vector2 pos2)
        {
            float x = Random.Range(pos1.x, pos2.x);
            float y = Random.Range(pos1.y, pos2.y);
            return new Vector2(x, y);
        }

        public static Vector2 ReturnRandomVector2()
        {
            return new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        }

        public static Vector2 ReturnRandomVector2InRange(float min, float max)
        {
            return new Vector2(Random.Range(min, max), Random.Range(min, max));
        }

        public static float RandomValueInRange(Vector2 range)
        {
            return Random.Range(range.x, range.y);
        }

        public static Vector3 SmoothApproach(Vector3 pastValue, Vector3 pastTargetValue, Vector3 targetValue, float speed)
        {
            float t = Time.deltaTime * speed;
            Vector3 v = (targetValue - pastTargetValue) / t;
            Vector3 f = pastValue - pastTargetValue + v;
            return targetValue - v + f * Mathf.Exp(-t);
        }

        public static float BobWave(float from, float to, float duration, float offset)
        {
            if (duration == 0f) return 0f;
            var arg4 = (to - from) * 0.5f;
            return from + arg4 + Mathf.Sin((((Time.time) + duration * offset) / duration) * 6.283185f) * arg4;
        }

        public static float BobWave(float from, float to, float duration, float offset, float lifeTime)
        {
            if (duration == 0f) return 0f;
            var arg4 = (to - from) * 0.5f;
            return from + arg4 + Mathf.Sin(((lifeTime + duration * offset) / duration) * 6.283185f) * arg4;
        }

        public static Color BobWaveColors(Color from, Color to, float duration, float offset)
        {
            float r, g, b, a;
            r = BobWave(from.r, to.r, duration, offset);
            g = BobWave(from.g, to.g, duration, offset);
            b = BobWave(from.b, to.b, duration, offset);
            a = BobWave(from.a, to.a, duration, offset);

            return new Color(r, g, b, a);
        }

        public static float SmoothApproach(float pastValue, float pastTargetValue, float targetValue, float speed)
        {
            Debug.Log("Target val: " + targetValue);
            Debug.Log("Past target val: " + pastTargetValue);

            float t = Time.deltaTime * speed;
            float v = (targetValue - pastTargetValue) / t;
            Debug.Log("V: " + v);
            float f = pastValue - pastTargetValue + v;
            Debug.Log("F: " + f);
            float final = targetValue - v + f * Mathf.Exp(-t);
            Debug.Log("Final: " + final);
            return final;
        }

        public static float SmoothApproach(float startVal, float endVal, float speed)
        {
            float min = Mathf.Min(startVal + speed, endVal);
            float max = Mathf.Max(startVal - speed, endVal);
            return startVal < endVal ? min : max;
        }

        public static void DebugDrawRect(Vector3 position, float width, float height, Color color)
        {
            var topLeft = position + new Vector3(-width * 0.5f, height * 0.5f);
            var topRight = position + new Vector3(width * 0.5f, height * 0.5f);
            var bottomLeft = position + new Vector3(-width * 0.5f, -height * 0.5f);
            var bottomRight = position + new Vector3(width * 0.5f, -height * 0.5f);

            Debug.DrawLine(bottomLeft, topLeft, color);
            Debug.DrawLine(topLeft, topRight, color);
            Debug.DrawLine(topRight, bottomRight, color);
            Debug.DrawLine(bottomRight, bottomLeft, color);
        }

        public static void DebugDrawCircle(Vector3 position, float radius, Color color)
        {
            var inc = 36f;
            var angle = 360f / inc;
            var a = position + Vector3.up * radius;

            for (int i = 0; i < inc; i++)
            {
                var b = a - position;
                b = UtilityFunctions.RotateVector(angle, b);
                b += position;
                Debug.DrawLine(a, b, color);
                a = b;
            }
        }

        public static void DebugDrawCross(Vector3 position, float size, Color color, float duration = 0f)
        {
            Vector3 top = position + Vector3.up * size * 0.5f;
            Vector3 bottom = position + Vector3.down * size * 0.5f;
            Vector3 right = position + Vector3.right * size * 0.5f;
            Vector3 left = position + Vector3.left * size * 0.5f;

            Debug.DrawLine(top, bottom, color, duration);
            Debug.DrawLine(right, left, color, duration);
        }

        public static Vector3 ScaleTransformVector(Vector3 vector, Vector3 scale)
        {
            return new Vector3(vector.x * scale.x, vector.y * scale.y);
        }

        public static bool CompareVectors(Vector3 a, Vector3 b, float angleError)
        {
            //if they aren't the same length, don't bother checking the rest.
            if (!Mathf.Approximately(a.magnitude, b.magnitude))
                return false;

            var cosAngleError = Mathf.Cos(angleError * Mathf.Deg2Rad);

            //A value between -1 and 1 corresponding to the angle.
            var cosAngle = Vector3.Dot(a.normalized, b.normalized);

            //The dot product of normalized Vectors is equal to the cosine of the angle between them.
            //So the closer they are, the closer the value will be to 1.  Opposite Vectors will be -1
            //and orthogonal Vectors will be 0.

            //If angle is greater, that means that the angle between the two vectors is less than the error allowed.
            return cosAngle >= cosAngleError ? true : false;
        }

        public static bool OnSameRenderingLayer(GameObject Object1, GameObject Object2, bool HigherLayerOkay, bool HigherOrderOkay)
        {
            int layer1 = Object1.GetComponent<Renderer>().sortingLayerID;
            int layer2 = Object2.GetComponent<Renderer>().sortingLayerID;

            if (layer1 != layer2)
            {
                if (layer2 < layer1)
                {
                    return false;
                }
                else if (layer2 > layer1)
                {
                    return HigherLayerOkay;
                }
            }

            if (layer1 == layer2)
            {
                int order1 = Object1.GetComponent<Renderer>().sortingOrder;
                int order2 = Object2.GetComponent<Renderer>().sortingOrder;
                if (order2 >= order1)
                {
                    return (order1 == order2 ? true : HigherOrderOkay);
                }
                else
                {
                    return false;
                }
            }

            return false;

        }

        public static RaycastHit2D Raycast(Vector2 origin, Vector2 direction, RaycastHit2D[] results, float distance = Mathf.Infinity, int layerMask = Physics2D.DefaultRaycastLayers, bool debug = false, Color debugColor = default(Color), float minDepth = -Mathf.Infinity, float maxDepth = Mathf.Infinity)
        {
            int num = Physics2D.RaycastNonAlloc(origin, direction, results, distance, layerMask, minDepth, maxDepth);

            if (debug)
                DebugExtension.DebugArrow(origin, direction.normalized * distance, debugColor);

            return num > 0 ? results[0] : new RaycastHit2D();
        }

        public static RaycastHit2D RaycastWithDebugHit(Vector2 origin, Vector2 direction, RaycastHit2D[] results, float distance = Mathf.Infinity, int layerMask = Physics2D.DefaultRaycastLayers, bool debug = false, Color debugColor = default(Color), Color hitColor = default(Color), float minDepth = -Mathf.Infinity, float maxDepth = Mathf.Infinity)
        {
            int num = Physics2D.RaycastNonAlloc(origin, direction, results, distance, layerMask, minDepth, maxDepth);
            RaycastHit2D hit = num > 0 ? results[0] : new RaycastHit2D();

            if (debug)
            {
                var color = hit ? hitColor : debugColor;
                DebugExtension.DebugArrow(origin, direction.normalized * distance, color);
            }

            return hit;
        }

        public static bool IsEqualToAnyInt(int valueToCompare, int value1, int value2)
        {
            return (valueToCompare == value1 || valueToCompare == value2);
        }

        public static bool IsEqualToAnyInt(int valueToCompare, int[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (valueToCompare == values[i])
                    return true;
            }
            return false;
        }

        public static bool IsEqualToAnyInt(int valueToCompare, int[] values, int oddValue)
        {
            if (valueToCompare == oddValue)
                return true;

            for (int i = 0; i < values.Length; i++)
            {
                if (valueToCompare == values[i])
                    return true;
            }

            return false;
        }

        public static bool IsEqualToAnyString(string valueToCompare, string[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (valueToCompare.Equals(values[i]))
                    return true;
            }
            return false;
        }

        public static bool IsEqualToAnyGameObject(GameObject valueToCompare, List<GameObject> values)
        {
            for (int i = 0; i < values.Count; i++)
            {
                if (valueToCompare == values[i])
                    return true;
            }
            return false;
        }

        public static bool IsEqualToAny<T>(T valueToCompare, T[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (valueToCompare.Equals(values[i]))
                    return true;
            }
            return false;
        }

        public static bool CheckForArrayValuesMismatch<T>(T[] valuesSet1, T[] valuestSet2)
        {
            for (int i = 0; i < valuesSet1.Length; i++)
            {
                T curVal = valuesSet1[i];
                bool valueExistsInOther = false;
                for (int j = 0; j < valuestSet2.Length; j++)
                {
                    if (curVal.Equals(valuestSet2[j]))
                    {
                        valueExistsInOther = true;
                        break;
                    }
                }
                if (!valueExistsInOther) return false;
            }
            return true;
        }

        public static void SetMeshRendererColor(MeshRenderer meshRenderer, Color color)
        {
            var material = new Material(meshRenderer.sharedMaterial);
            material.color = color;
            meshRenderer.material = material;
        }

        public static string GetSceneNameFromScenePath(string scenePath)
        {
            // Unity's asset paths always use '/' as a path separator
            var sceneNameStart = scenePath.LastIndexOf("/", System.StringComparison.Ordinal) + 1;
            var sceneNameEnd = scenePath.LastIndexOf(".", System.StringComparison.Ordinal);
            var sceneNameLength = sceneNameEnd - sceneNameStart;
            return scenePath.Substring(sceneNameStart, sceneNameLength);
        }

        public static void BreakOnBool(bool br)
        {
            if (br)
                Debug.Break();
        }

        public static bool WithinPointRect(Vector3 pointEvaluate, Vector3 inRectPoint, Vector2 size)
        {
            return WithinPointRect(pointEvaluate, inRectPoint, size.x, size.y);
        }

        public static bool WithinPointRect(Vector3 pointEvaluate, Vector3 inRectPoint, float width, float height, bool debug = false, Color color = default(Color), Color inColor = default(Color))
        {
            float xLowerBound = inRectPoint.x - width * 0.5f;
            float xUpperBound = inRectPoint.x + width * 0.5f;
            float yLowerBound = inRectPoint.y - height * 0.5f;
            float yUpperBound = inRectPoint.y + height * 0.5f;

            bool inRect = pointEvaluate.x > xLowerBound && pointEvaluate.x < xUpperBound && pointEvaluate.y > yLowerBound && pointEvaluate.y < yUpperBound;

            if (debug)
            {
                if (inRect)
                    UtilityFunctions.DebugDrawRect(pointEvaluate, width, height, inColor);
                else
                    UtilityFunctions.DebugDrawRect(pointEvaluate, width, height, color);
            }

            return inRect;
        }

        public static Color RandomColor()
        {
            return new Color(Random.value, Random.value, Random.value, 1f);
        }

        public static T Choose<T>(T a, T b, params T[] p)
        {
            int random = Random.Range(0, p.Length + 2);
            if (random == 0) return a;
            if (random == 1) return b;
            return p[random - 2];
        }

        public static void SetActiveEnMasse(bool active, GameObject[] objects)
        {
        foreach (GameObject g in objects)
        {
            if (g == null)
            {
                Debug.LogError("Missing value in array!");
                continue;
            }
                g.SetActive(active);
            }
        }
        //Not used publically
        private static float NextGaussian()
        {
            float v1, v2, s;
            do
            {
                v1 = 2.0f * Random.Range(0f, 1f) - 1.0f;
                v2 = 2.0f * Random.Range(0f, 1f) - 1.0f;
                s = v1 * v1 + v2 * v2;
            } while (s >= 1.0f || s == 0f);

            s = Mathf.Sqrt((-2.0f * Mathf.Log(s)) / s);

            return v1 * s;
        }

        //used publically

        public static float RandomGaussian(float mean, float standard_deviation)
        {
            return mean + NextGaussian() * standard_deviation;
        }

        public static float RandomGaussian(float mean, float standard_deviation, float min, float max)
        {
            float x;
            do
            {
                x = RandomGaussian(mean, standard_deviation);
            } while (x < min || x > max);
            return x;
        }


        //**Cukia I know this is probably bad, sorry**

        //To turn off previous tween if needed
        private static Tween _textUnfold;
        
        //Piggback completed text unfolded tween to use: UtilityFunctions.UnfoldTextEndCall
        public delegate void UnfoldTextEndCall();

        public static Tween UnfoldTextOverTime(float time, Text textBox, string text, Ease easeType = Ease.Linear, UnfoldTextEndCall endCall = null, string startingString = "")
        {
        
            if (_textUnfold != null && !_textUnfold.IsComplete()) _textUnfold.Complete();

            textBox.text = startingString;
            _textUnfold = textBox.DOText(text, time, false).SetEase(easeType).OnComplete(() => {endCall();});
        
            return _textUnfold;
        }

        //////////New\\\\\\\\\\
        public static int GetEnumTypeValueCount(System.Type a)
        {
            return System.Enum.GetValues(a).Length;
        }
        
        //Use typeOf() with enum type you pass
        public static System.Array GetEnumTypeArrayValues(System.Type enumType)
        {
            return System.Enum.GetValues(enumType);
        }

        public static List<T> ReturnEnumRandomValues<T>(System.Type enumType, int number, T toAvoid)
        {
        Debug.Log("Called");
            List<T> returnedValues = new List<T>();
            int count = GetEnumTypeValueCount(enumType);
            System.Array values = GetEnumTypeArrayValues(enumType);

            for (int i =0; i < number; i++)
            {
                T holder = (T)values.GetValue(Random.Range(0, count));
                while(holder.Equals(toAvoid) || IsEqualToAny<T>(holder, returnedValues.ToArray()))
                {
                    holder = (T)values.GetValue(Random.Range(0, count));
                }
                returnedValues.Add(holder);
            }

            return returnedValues;
        }

        public static List<string> ConvertEnumsToStrings<T>(T[] values)
        {
            List<string> textValues = new List<string>();
            for(int j= 0; j< values.Length;j++)
            {
                textValues.Add(values[j].ToString());
            }
            return textValues;
        }
    }