﻿using UnityEngine;
using System.Collections;

public class UtilitySetTextMeshLayer : MonoBehaviour {

	public string layerString;
	public int layerOrder;
	// Use this for initialization
	void Start () {
		GetComponent<Renderer>().sortingLayerName = layerString;
		GetComponent<Renderer>().sortingOrder = layerOrder;
	}
}
