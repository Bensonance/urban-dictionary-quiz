﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Utility_ParticleColorBlend : MonoBehaviour {

    public Color GoalColor;
    public float BlendTime;
    public bool BlendOnStart;
    private ParticleSystem _partSystem;
    private Color _startColor;
	// Use this for initialization
	void Start () {
        _partSystem = GetComponent<ParticleSystem>();
        _startColor = _partSystem.main.startColor.color;
        if (BlendOnStart)
            StartCoroutine(BlendColor(GoalColor, BlendTime));
	}

    public void StartBlend()
    {
        StartCoroutine(BlendColor(GoalColor, BlendTime));
    }
	

    IEnumerator BlendColor(Color c, float time)
    {
        float t =0f;
        var main = _partSystem.main;
        while (t <time)
        {
            main.startColor = Color.Lerp(_startColor, GoalColor, t / time);
            t += Time.deltaTime;
            yield return null;
        }
    }
    
}
