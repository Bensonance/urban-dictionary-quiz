using UnityEngine;

namespace Nyamakop
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<T>();

                    if (_instance != null)
                        DontDestroyOnLoad(_instance.gameObject);
                }
     
                return _instance;
            }
        }

        public virtual void Awake()
        {
            if (_instance == null)
            {
                _instance = this as T;
                DontDestroyOnLoad(this);
            }
            else
            {
                DestroyImmediate(gameObject);
                return;
            }
        }
    }
}
