﻿[System.Serializable]
public class SpriteProperties
{
    public string SortingLayerName;
    public int SortingLayerOrder;
    public int SortingLayerIndex;

    public SpriteProperties(string name, int order)
    {
        SortingLayerName = name;
        SortingLayerOrder = order;
    }

}