﻿using UnityEngine;
using System.Collections;

public class FadeInUtility : MonoBehaviour
{

    private SpriteRenderer _render;
    public float Speed;
    public Color StartColor;
    public Color EndColor;
    private bool _isDone;
    private bool _canFade;
    public bool SelfStarting;
    public KeyCode PressToFade;

    public bool IsFaded
    {
        get
        {
            _render = GetComponent<SpriteRenderer>();
            return(_render.color.a == 0f);
        }
    }
    
    // Use this for initialization
    void Start()
    {
        _render = GetComponent<SpriteRenderer>();

        if (SelfStarting)
        {
            _render.color = StartColor;
            _canFade = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(PressToFade))
        {
            StartFade(-1f);
        }
        if (_isDone || !_canFade)
            return;

        Color cur = _render.color;
        cur = Color.Lerp(cur, EndColor, Speed);
        _render.color = cur;
        if (_render.color.Equals(EndColor)) _isDone = true;
        
    }

    public void StartFade(bool delay)
    {
        _render = GetComponent<SpriteRenderer>();
        if (delay)
        {
            StartCoroutine(DelayStart(2.25f));
        }
        else
        {
            _canFade = true;
            //_render.color = EndColor;
        }
    }

    public void Setup(float speed, Color startColor, Color endColor)
    {
        _render = GetComponent<SpriteRenderer>();
        Speed = speed;
        StartColor = startColor;
        EndColor = endColor;
    }

    public void Setup(bool swapStartEndColors)
    {

        _render = GetComponent<SpriteRenderer>();
        if(swapStartEndColors)
        {
            Color c = EndColor;
            EndColor = StartColor;
            StartColor = c;
        }
    }

    public void ResetFade()
    {
        _render = GetComponent<SpriteRenderer>();
        _canFade = false;
        _isDone = false;
        _render.color = StartColor;
    }

    public void StartFade(float speedChange = -1f)
    {
        if (speedChange != -1f) Speed = speedChange;
        _canFade = true;
    }

    public void StopRoutine()
    {
        StopAllCoroutines();
        _canFade = false;
    }

    public IEnumerator DelayStart(float period)
    {
        yield return new WaitForSeconds(period);
        _canFade = true;
    }

    public void AlphaToLimit(float limit)
    {
        
        _render = GetComponent<SpriteRenderer>();
        _render.color = new Color(_render.color.r, _render.color.g, _render.color.b, limit);
    }
}
