﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility_MaintainOffset : MonoBehaviour {


    public GameObject TargetObject;
    public Vector3 Offset;
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = TargetObject.transform.position + Offset;
	}

    public void SetupScript(GameObject target, Vector3 offset)
    {
        TargetObject = target;
        Offset = offset;
    }
}
