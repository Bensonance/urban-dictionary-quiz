﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;
using DG.Tweening;

/// <summary>
/// Requires InControl to function properly.
/// </summary>
/// 
//[RequireComponent(typeof(SpriteRenderer), typeof(AnimationManualUtility))]
public class ControlsPromptUtility : MonoBehaviour
{

    public CrossPlatformSpriteHolder ActionPrompt, ActionLabel;
    private Sprite[] _actionPrompt, _actionLabel;
    public bool CheckInputChange, DisplayOnStart;

    [Header("Action Prompt Local Offsets")]
    public bool NoOffsets;
    public Vector3 PCOffset, ConsoleOffset;
    private InputDeviceStyle _lastInput = InputDeviceStyle.Unknown;
    private AnimationManualUtility _animManual;

    [Space]

    [Header("Action Label Local Offsets")]
    public bool NoLabelOffsets;
    private SpriteRenderer _render, _renderLabel;
    private AnimationManualUtility _animLabelManual;
    public Vector3 PCLabelOffset, ConsoleLabelOffset;
    private float _startScale;

    public InputDeviceStyle AssumedPlatform = InputDeviceStyle.Unknown;

    private void Start()
    {
        //if (GameManager.Instance != null) _lastInput = AssumedPlatform != InputDeviceStyle.Unknown ? AssumedPlatform : GameManager.Instance.LastInputType;
        _animManual = GetComponent<AnimationManualUtility>();
        _startScale = transform.localScale.x;
        _animLabelManual = transform.GetChild(0).GetComponent<AnimationManualUtility>();
        UpdateSprites(_lastInput);


        _render = GetComponent<SpriteRenderer>();
        if (_render != null) _render.enabled = false;

        _renderLabel = transform.GetChild(0).GetComponent<SpriteRenderer>();
        if (_renderLabel != null) _renderLabel.enabled = false;
        if (DisplayOnStart) RenderPrompt();

    }

    // Update is called once per frame
    void Update()
    {
        if (CheckInputChange)
        {
            var newControl = _lastInput;

            //if (GameManager.Instance != null)
               // newControl = GameManager.Instance.LastInputType;

            if (newControl != _lastInput)
            {
                _lastInput = newControl;
                Sequence seq = DOTween.Sequence();
                seq.Append(transform.DOScale(Vector3.zero, 0.5f).SetEase(Ease.OutBounce));
                seq.AppendCallback(() =>
                {
                    UpdateSprites(_lastInput, true);

                });
                seq.Append(transform.DOScale(Vector3.one * _startScale, 1f).SetEase(Ease.OutElastic));

            }
        }
    }

    void UpdateOffsets()
    {
        if (!NoOffsets)
        {
            transform.localPosition = ActionPrompt.CurrentPlatformClass == PlatformClass.Console ? ConsoleOffset : PCOffset;
        }

        if (!NoLabelOffsets)
        {
            transform.GetChild(0).localPosition = ActionLabel.CurrentPlatformClass == PlatformClass.Console ? ConsoleLabelOffset : PCLabelOffset;
        }
    }

    void UpdateSprites(InputDeviceStyle control, bool updateTimings = false)
    {
        _actionPrompt = ActionPrompt.GetPlatformSprites(control);
        _actionLabel = ActionLabel.GetPlatformSprites(control);
        _animManual.UpdateSprites(_actionPrompt);
        _animLabelManual.UpdateSprites(_actionLabel);

        if (updateTimings)
        {
            _animManual.RealignTiming();
            _animLabelManual.RealignTiming();
        }
        UpdateOffsets();
    }

    void DisplayPrompt()
    {
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one * _startScale, 1.5f).SetEase(Ease.OutElastic);

        RenderPrompt();
    }

    void RenderPrompt()
    {
        _render.enabled = true;
        _renderLabel.enabled = true;
    }
}
