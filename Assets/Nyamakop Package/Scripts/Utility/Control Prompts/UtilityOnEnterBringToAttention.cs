﻿using UnityEngine;
using System.Collections;

public class UtilityOnEnterBringToAttention : MonoBehaviour {

    public bool ChangeValue;
    public Vector2 ValueStartEnd;
    public GameObject[] Children;
    public string SortingLayer;
    public int SortingLayerOrderStart;
    public int SortingLayerOrderEnd;
    private float _value;
    public bool ChangeLayers;
    private int[] _layersStorage;
    public bool ChangeChildren;

    private bool _setupComplete = false;
    private bool _isReturning = false;
    private bool _returnComplete = false;

	// Use this for initialization
	void Start () {
        _value = ValueStartEnd.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_setupComplete)
        {
            _setupComplete = true;
            ChangeValues(_value, SortingLayer, SortingLayerOrderStart);
            ChangeLayersActual(false);
        }

        if(_isReturning && !_returnComplete)
        {
            _value = Mathf.Lerp(_value, ValueStartEnd.y, 0.05f);
            ChangeValues(_value, SortingLayer, SortingLayerOrderEnd);
            ChangeLayersActual(true);
            if (_value > 0.99)
            {
                _returnComplete = true;
            }
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if(coll.tag == "Player")
        {
            _isReturning = true;
        }
    }
    private void ChangeLayersActual(bool LayersEnabled)
    {
        Transform[] colliders = GetComponentsInChildren<Transform>();
        if (_layersStorage == null) _layersStorage = new int[colliders.Length];
        //Debug.Log("LENGTH: " + renders.Length);
        for (int i = 0; i < colliders.Length; i++)
        {
            var curID = colliders[i];
            if (!LayersEnabled)
            {
                _layersStorage[i] = curID.gameObject.layer;
                curID.gameObject.layer = 2;
            }
            else
            {
                curID.gameObject.layer = _layersStorage[i];
            }
        }
        gameObject.layer = 0;
    }

    private void ChangeValues(float value, string SortingLayer, int SortingOrderPos)
    {
        MeshRenderer[] renders = GetComponentsInChildren<MeshRenderer>();
        if (renders.Length <= 0)
        {
            renders = new MeshRenderer[10];
            for(int j = 0; j < Children.Length; j++)
            {
                renders[j] = Children[j].GetComponent<MeshRenderer>();
            }
        }
        if (renders.Length <= 0)
            return;
        foreach (MeshRenderer r in renders)
        {
            float h, s, v;
            Color.RGBToHSV(r.material.color, out h, out s, out v);
            v = value;
            r.material.color = Color.HSVToRGB(h, s, v);
            r.sortingLayerName = SortingLayer;
            r.sortingOrder = SortingOrderPos;
        }
    }
}
