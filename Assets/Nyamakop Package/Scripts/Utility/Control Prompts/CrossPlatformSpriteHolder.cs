﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

[System.Serializable]
public class CrossPlatformSpriteHolder
{
    //Default sprites used when it's the same sprite on all platforms.

    public Sprite[] DefaultSprites, WindowsSprites, MacSprites, SwitchSprites, XboxOneSprites, PS4Sprites;
    public PlatformClass CurrentPlatformClass;

    public Sprite[] GetPlatformSprites()
    {
        if (DefaultSprites.Length > 0) return DefaultSprites;

        switch (Application.platform)
        {
            case RuntimePlatform.WindowsPlayer:
                CurrentPlatformClass = PlatformClass.PC;
                return WindowsSprites;

            case RuntimePlatform.OSXPlayer:
                CurrentPlatformClass = PlatformClass.PC;
                return MacSprites;

            case RuntimePlatform.Switch:
                CurrentPlatformClass = PlatformClass.Console;
                return SwitchSprites;

            case RuntimePlatform.XboxOne:
                CurrentPlatformClass = PlatformClass.Console;
                return XboxOneSprites;

            case RuntimePlatform.PS4:
                CurrentPlatformClass = PlatformClass.Console;
                return PS4Sprites;

            default:
                CurrentPlatformClass = PlatformClass.Console;
                return XboxOneSprites;
        }
    }

    //Gets platform sprites based on controller plugged in
    public Sprite[] GetPlatformSprites(InputDeviceStyle controlDescription)
    {
        if (DefaultSprites.Length > 0) return DefaultSprites;

        RuntimePlatform controllerOnPlatform = GetControllerPlatform(controlDescription, Application.platform);
        
        // Debug.Log("Control: " + controllerOnPlatform);
        
        switch (controllerOnPlatform)
        {
            case RuntimePlatform.WindowsPlayer:
            case RuntimePlatform.WindowsEditor:
                CurrentPlatformClass = PlatformClass.PC;
                return WindowsSprites;

            case RuntimePlatform.OSXPlayer:
            case RuntimePlatform.OSXEditor:
                CurrentPlatformClass = PlatformClass.PC;
                return MacSprites;

            case RuntimePlatform.Switch:
                CurrentPlatformClass = PlatformClass.Console;
                return SwitchSprites;

            case RuntimePlatform.XboxOne:
                CurrentPlatformClass = PlatformClass.Console;
                return XboxOneSprites;

            case RuntimePlatform.PS4:
                CurrentPlatformClass = PlatformClass.Console;
                return PS4Sprites;

            default:
                CurrentPlatformClass = PlatformClass.PC;
                return XboxOneSprites;
        }
    }

    public static RuntimePlatform GetControllerPlatform(InputDeviceStyle controller, RuntimePlatform operatingPlatform)
    {
        switch(controller)
        {
            case InputDeviceStyle.Xbox360:
            case InputDeviceStyle.XboxOne:
                return RuntimePlatform.XboxOne;
            
            case InputDeviceStyle.PlayStation2:
            case InputDeviceStyle.PlayStation3:
            case InputDeviceStyle.PlayStation4:
                return RuntimePlatform.PS4;
            
            case InputDeviceStyle.NintendoSwitch:
                return RuntimePlatform.Switch;
            
            default:
                return operatingPlatform;
        }
    }
}

public enum PlatformClass { PC, Console };