﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class SnapOption : ScriptableObject
{
    public string Tag;
    public bool SnapScale;
    public bool SnapPosition;
}

[ExecuteInEditMode]
public class GridTilingTool : MonoBehaviour
{
    public float Width;
    public float Height;
    public Color Color = Color.white;
    public bool DrawGrid;
    public bool Snap;
    
    public List<string> SnapTags;
    public List<bool> TagSnapScale;
    public List<bool> TagSnapPosition;
    
#if UNITY_EDITOR
    void Start()
    { 
        if (!Application.isPlaying)
        {
            SceneView.onSceneGUIDelegate -= GridUpdate;
            SceneView.onSceneGUIDelegate += GridUpdate;
        }
        else 
            DrawGrid = false;
    }

    void OnEnable()
    { 
        if (!Application.isPlaying)
        {
            SceneView.onSceneGUIDelegate -= GridUpdate;
            SceneView.onSceneGUIDelegate += GridUpdate;
        }
    }

    void GridUpdate(SceneView sceneView)
    {
        Event e = Event.current;
        
        if (e.type == EventType.KeyDown && e.shift && e.keyCode == KeyCode.S)
        {
            Snap = !Snap;
            print("Snap: " + Snap);
        }

        if (!Snap || (Width == 0|| Height == 0))
            return;

        if (e.type == EventType.MouseUp)
        {
            foreach (GameObject obj in Selection.gameObjects)
            {
                bool snap = SnapThis(obj.transform);
                if (snap)
                {
                    int index = SnapTags.IndexOf(obj.transform.tag);
                    bool snapScale = TagSnapScale[index];
                    bool snapPosition = TagSnapPosition[index];

                    switch (Tools.current)
                    {
                        case Tool.Move:
                            

                            SnapObject(obj.transform, snapPosition, snapScale);
                            break;

                        case Tool.Scale:
                            SnapObject(obj.transform, snapPosition, snapScale);
                            break;

                        case Tool.Rect:
                            SnapObject(obj.transform, snapPosition, snapScale);
                            break;
                    }
                }
            }
        }
    }

    void OnDestroy()
    {
        if (!Application.isPlaying)
            SceneView.onSceneGUIDelegate -= GridUpdate;
    }
#endif

    void SnapObject(Transform t, bool snapPosition = true, bool snapScale = false)
    {
        if (snapPosition)
            t.position = SnapVector(t.position);

        if (snapScale)
        {
            var s = (Vector2)SnapVector(t.localScale);
            t.localScale = s.magnitude < float.Epsilon ? t.localScale : new Vector3(s.x, s.y, 1f);
        }
    }

    public Vector3 SnapVector(Vector3 snapVector)
    {
        var x1 = Mathf.Floor(snapVector.x / Width) * Width;
        var x2 = Mathf.Ceil(snapVector.x / Width) * Width;
        // x1 = x1 < float.Epsilon ? x2 : x1;

        var y1 = Mathf.Floor(snapVector.y / Height) * Height;
        var y2 = Mathf.Ceil(snapVector.y / Height) * Height;
        // y1 = y1 < float.Epsilon ? y2 : y1;

        var x = Mathf.Abs(snapVector.x - x1) < Mathf.Abs(snapVector.x - x2) ? x1 : x2;
        var y = Mathf.Abs(snapVector.y - y1) < Mathf.Abs(snapVector.y - y2) ? y1 : y2;

        return new Vector3(x, y, snapVector.z);
    }

    bool SnapThis(Transform t)
    {
        return SnapTags.Any(_ => _ == t.tag);
    }

    void OnDrawGizmos()
    {
        if (!DrawGrid || !Snap)
            return;

        Vector3 pos = Camera.current.transform.position;
        float ySize = Mathf.Min(Camera.current.orthographicSize, 8000f);
        float xSize = Mathf.Min(ySize * Camera.current.aspect, 8000f * Camera.current.aspect);

        Gizmos.color = Color;
        
        for (float y = pos.y - ySize; y < pos.y + ySize; y += Height)
        {
            Gizmos.DrawLine(new Vector3(-1000000.0f, Mathf.Floor(y / Height) * Height, -8.0f),
                new Vector3(1000000.0f, Mathf.Floor(y / Height) * Height, -8.0f));
        }
    
        for (float x = pos.x - xSize; x < pos.x + xSize; x += Width)
        {
            Gizmos.DrawLine(new Vector3(Mathf.Floor(x / Width) * Width, -1000000.0f, -8.0f),
                new Vector3(Mathf.Floor(x / Width) * Width, 1000000.0f, -8.0f));
        }
    }
}