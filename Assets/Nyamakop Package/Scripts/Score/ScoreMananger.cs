﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Nyamakop;

public class ScoreMananger : MonoBehaviour{

    public int CurrentScore;
    public int ResetScorePoint;
    public Text ScoreDisplay;
    private Utility_UI_EffectsStack _effects;

    public string ScorePrefix;

    public int LowerTriggerPoint, UpperTriggerPoint;

    public delegate void LowerTriggerPointCall();
    public delegate void UpperTriggerPointCall();

    public LowerTriggerPointCall LowerTrigger;

    public Color StandardColor;

    void Start()
    {
        _effects = GetComponent<Utility_UI_EffectsStack>();
        UpdateScore();
        
    }

    public void ChangeScore(int scoreDifference, bool setScore = false, bool changeResetScore = false)
    {
        CurrentScore = setScore ? scoreDifference : scoreDifference+ CurrentScore;
        if (changeResetScore) ResetScorePoint = CurrentScore;
        UpdateScore(true, scoreDifference<0);
    }

    //Add effects: Scale pulse, score ticking up to new score, color change etc.
    public void UpdateScore(bool effects = false, bool negativeChange = false)
    {
        if (ScoreDisplay == null) return;
        ScoreDisplay.text = ScorePrefix + CurrentScore;
        if (effects)
        {
            _effects.ScalePulse(Vector3.one * 2f, 0.1f, 0.2f);
            _effects.FlashColorChangeStart(UrbanDictionaryWordManager.RightWordFeedbackTime, 0.3f, negativeChange ? Color.red : Color.green, Color.white, StandardColor);
        }

        if (CurrentScore <= LowerTriggerPoint && LowerTrigger != null) LowerTrigger();
    }

    public void ResetScore()
    {
        CurrentScore = ResetScorePoint;
        UpdateScore();
    }
}
