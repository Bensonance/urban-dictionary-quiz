﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class ObliqueCollision : MonoBehaviour {

    private Collider2D _collider;
    public bool DestroyOnWall;
    public LayerMask WallLayer;
	// Use this for initialization
	void Awake () {
        _collider = GetComponent<Collider2D>();
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (WallLayer ==  (WallLayer | 1 << collision.gameObject.layer))
        {
            if (DestroyOnWall) Destroy(this.gameObject);
        }
    }


}
