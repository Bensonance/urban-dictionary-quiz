﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nyamakop;

public class ObliqueMovement_Player : ObliqueMovement {

    public float MinSpeed, MinAcceleration, MaxAcceleration, MaxSpeedCached;

    private static ObliqueMovement_Player _instance;
    public static ObliqueMovement_Player Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ObliqueMovement_Player>();

                if (_instance != null)
                    DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    public override void Awake()
    {   
        if (_instance == null)
        {
            _instance = this as ObliqueMovement_Player;
            DontDestroyOnLoad(this);
        }
        else
        {
            DestroyImmediate(gameObject);
            return;
        }
        base.Awake();
    }

}
