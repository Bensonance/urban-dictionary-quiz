﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LifePlayerCollissionControl : LifeCollisionControl {

    public Action OnLifeDamage;
    
    DamageTrigger playerDamageEffects;

    protected override void Awake()
    {
        base.Awake();
        _effects = transform.parent.GetComponentInChildren<Utility_EffectsStack>();
        playerDamageEffects += _effects.DamageColorFlash;
        playerDamageEffects += _effects.CameraShakeMedium;
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (UtilityFunctions.IsEqualToAnyString(collision.tag, CollisionNames))
        {
            if(OnLifeDamage != null)OnLifeDamage();
            playerDamageEffects();
            base.OnTriggerEnter2D(collision);
        }
    }
    
}
