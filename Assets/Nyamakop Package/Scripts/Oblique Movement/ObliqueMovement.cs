﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObliqueMovement : MonoBehaviour {

    public float MaxSpeed, Acceleration, Friction;
    private Vector2 _moveDirection;
    private float _currentSpeed
    {
        get
        {
            return _body.velocity.magnitude;
        }

        set
        {
            _body.velocity = _body.velocity.normalized * value;
        }
    }
    private Rigidbody2D _body;
    // Use this for initialization
    public virtual void Awake () {
        _body = GetComponent<Rigidbody2D>();
	}

    // Update is called once per frame
    virtual protected void Update () {

        MoveTransform();
        ApplyFriction();
	}

    virtual public void SetSpeed(Vector2 direction, float speed, bool setToMaxSpeed = false)
    {
        if (setToMaxSpeed) speed = MaxSpeed;
        _body.velocity = direction * speed;
    }

    virtual public void Accelerate(Vector2 direction, float acceleration = 0f)
    {
        if (acceleration == 0f) acceleration = Acceleration;
        _body.AddForce(direction * acceleration);
        ClampSpeed();
    }

    virtual public void HaltMovement()
    {
        _body.velocity = Vector2.zero;
    }

    virtual protected void ApplyFriction()
    {
        if(_currentSpeed > Friction)
        {
            _currentSpeed -= Friction;
        }
        else
        {
            _currentSpeed = 0f;
        }
    }

    virtual protected void MoveTransform()
    {
        //transform.Translate(_currentSpeed);
    }

    virtual protected void ClampSpeed()
    {
        _currentSpeed = Mathf.Clamp(_currentSpeed, -MaxSpeed, MaxSpeed);
    }
    
}
