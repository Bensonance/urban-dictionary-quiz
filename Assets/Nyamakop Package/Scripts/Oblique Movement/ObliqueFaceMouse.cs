﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObliqueFaceMouse : MonoBehaviour {

    [HideInInspector]
    public Vector2 CurrentDirection;
    public Vector3 GoalPosition;
    public FaceType ThingToFace = FaceType.Mouse;

    private void Update()
    {
        FaceMouse();
    }

    void FaceMouse()
    {
        var objectPos = ThingToFace == FaceType.Mouse ? Camera.main.WorldToScreenPoint(transform.position) : GoalPosition;
        CurrentDirection = Input.mousePosition - objectPos;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(CurrentDirection.y, CurrentDirection.x) * Mathf.Rad2Deg));
    }

    public enum FaceType { Mouse, Goal}
}
