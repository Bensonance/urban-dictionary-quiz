﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

[RequireComponent(typeof(ObliqueMovement))]
public class ObliqueMovementInputControl : MonoBehaviour {

    private MyCharacterActions _inputs;
    private ObliqueMovement _mover;
	// Use this for initialization
	void Awake () {
        _inputs = new MyCharacterActions();
        _inputs.Initialize();
        _mover = GetComponent<ObliqueMovement>();
	}
	
	// Update is called once per frame
	void Update () {
        DetectMovementInput();
	}

    void DetectMovementInput()
    {
        if(_inputs.Up)
        {
            _mover.Accelerate(Vector2.up);
        }

        if (_inputs.Down)
        {
            _mover.Accelerate(Vector2.down);
        }

        if (_inputs.Left)
        {
            _mover.Accelerate(Vector2.left);
        }

        if (_inputs.Right)
        {
            _mover.Accelerate(Vector2.right);
        }
    }
}
