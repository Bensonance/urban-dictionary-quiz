﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelDressing_Wiggle : MonoBehaviour
{
    public static GameObject ParentGameObject;

    // [FMODUnity.EventRef]
    // public string RustleSoundEvent;
    
    // [FMODUnity.EventRef]
    // public string WiggleSoundEvent;s

    public bool CheckRenderLayer;
    private bool _isWiggling;
    private float _curWiggle;
    public float StartWiggleSize;
    public float PercDecay;
    public float _startAngle;

    private float _curOffset;
    private float _goalWiggle;

    public Vector2 SpeedModRange;
    private float _instanceSpeedMod;
    public Vector2 SizeModRange;
    public Vector2 TimeRange;
    private float _curTime;
    private float _instanceSizeMod;
    private int _DirMod;

    public float DashSizeMod = 2f;
    public float DashDecayMod = 0.99f;
    private float _startDecayMod;

    private float _curScale;
    private float _offset;
    [HideInInspector]
    public bool WindSet;

    [HideInInspector]
    public Vector3 StartRenderSize;

    private float PlayerSpecificShakeMod = 1.5f;
    private Vector2 WiggleSizeRange = new Vector2(2.5f, 90f);

    public bool AddPolygonCollider;
    private Renderer _render;
    public bool AllowWiggle = true, ChangeRenderLayer, unParentOnPlay;

    void Start()
    {
        if (unParentOnPlay)
        {
            transform.parent = null;
            
            if (ParentGameObject == null)
                ParentGameObject = new GameObject("Flower Parent");

            transform.parent = ParentGameObject.transform;
        }
            

        _render = GetComponent<Renderer>();

        if (AddPolygonCollider && GetComponent<Collider2D>() == null)
            gameObject.AddComponent<PolygonCollider2D>();

        GetComponent<Collider2D>().isTrigger = true;

        if (ChangeRenderLayer)
        {
            SpriteRenderer render = GetComponent<SpriteRenderer>();
            bool change = UtilityFunctions.Chance(50);
            render.sortingLayerName = change ? "Foreground" : "Middleground";
            render.sortingOrder = change ? 4 : 1;
        }

        _curScale = 1f;

        //StartRenderSize = GetComponent<Renderer>().bounds.size;
        //transform.localScale = new Vector3(Mathf.Round(Random.Range(1, 1)) * transform.localScale.x, transform.localScale.y);

        if (transform.localScale.x == 0)
            transform.localScale = new Vector3(1f, transform.localScale.y);

        _startAngle = transform.eulerAngles.z;

        //random != null ? Random.Range(random.RotationRange.x, random.RotationRange.y) : Random.Range(0f, 0f);

        _startDecayMod = PercDecay;
        _instanceSpeedMod = Random.Range(SpeedModRange.x, SpeedModRange.y);
        _instanceSizeMod = Random.Range(SizeModRange.x, SizeModRange.y);
        _curTime = Random.Range(TimeRange.x, TimeRange.y);

        StartWiggleSize *= Random.Range(0.85f, 1.25f);
        StartWiggleSize = Mathf.Clamp(StartWiggleSize, WiggleSizeRange.x, WiggleSizeRange.y);
        _offset = Random.Range(0f, 1f);

        /*Color col = GetComponent<SpriteRenderer>().color;
        float h;
        float s;
        float v;
        Color.RGBToHSV(col, out h, out s, out v);
        v = GetComponent<SpriteRenderer>().sortingLayerName == "Background" ? 160f / 255f: 220f / 255f;
        col = Color.HSVToRGB(h, s, v);
        GetComponent<SpriteRenderer>().color = col;    */
        
    }

    void Update()
    {
        if (!AllowWiggle)
            return;

        if (!_render.isVisible || !_isWiggling)
            return;

        float rot = 0f;

        if (_isWiggling)
        {
            _curWiggle = Mathf.Lerp(_curWiggle, _goalWiggle, 0.1f);
            rot = UtilityFunctions.BobWave(-_curWiggle, _curWiggle, _curTime, _offset);
            _goalWiggle *= PercDecay;
        }

        //transform.localScale = new Vector3(_curScale, 1f, 0f);

        transform.localEulerAngles = new Vector3(0f, 0f, _startAngle + rot);

    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            // FMODUnity.RuntimeManager.PlayOneShot(WiggleSoundEvent);
            
            _isWiggling = true;
            _goalWiggle = StartWiggleSize * _instanceSizeMod;

            if (_goalWiggle <= 4f)
                _goalWiggle *= PlayerSpecificShakeMod;

            _curScale = 1f;
            if (true)
            {
                _goalWiggle *= DashSizeMod;
                PercDecay = DashDecayMod;
            }
            else
            {
                PercDecay = _startDecayMod;
            }

            _curOffset = transform.localEulerAngles.z;
        }
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        if (coll.name.Contains("Air"))
        {

            TriggerWiggle(0f);
        }
    }

    public void TriggerWiggle(float wiggleBonus)
    {
        _isWiggling = true;
        _goalWiggle = (StartWiggleSize * _instanceSizeMod) + wiggleBonus;
        _curScale = 1f;
        PercDecay = _startDecayMod;
        _curOffset = transform.localEulerAngles.z;

        WindSet = true;
    }

    void OnParticleCollision()
    {
        float goalWiggle = (StartWiggleSize * _instanceSizeMod) * -Random.Range(0.75f, 0.99f);
        if (_goalWiggle < goalWiggle * 1.1f || _goalWiggle < 0.1f)
            TriggerWiggle(goalWiggle);
    }
}
