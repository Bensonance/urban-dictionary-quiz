﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WavySprite:MonoBehaviour{

	[HideInInspector]
	public  MeshRenderer mr;
	private Material mat;
	private MeshFilter mf;
	private Mesh mesh;
	private List<Vector3> vertices = new List<Vector3>(200);
	private List<Vector3> uv0 = new List<Vector3>(200);
	private List<Vector3> uv1 = new List<Vector3>(200);
	private List<Color> colors = new List<Color>(200);
	private int[] triangles;

	public Sprite sprite;
	private Sprite _sprite;

	public Color tint = Color.white;
	private Color _tint;

	[Range(0,30)]
	public int divisionsX = 2;
	private int _divisionsX;

	[Range(0,30)]
	public int divisionsY = 2;
	private int _divisionsY;

	public enum waveDirections {Vertical, Horizontal};
	public waveDirections waveDirection = waveDirections.Vertical;
	private waveDirections _waveDirection;

	public enum objSides{Top, Right, Bottom, Left, None}
	public objSides staticSide = objSides.None;
	private objSides _staticSide;

	[Range(-10,10)]
	public float waveFrequency = 10.0f;
	private float _waveFrequency;

	[Range(0.0f,1.0f)]
	public float waveForce = 0.03f;
	private float _waveForce;

	[Range(0.0f,10.0f)]
	public float waveSpeed = 0.5f;
	private float _waveSpeed;

	float meshWidth = 1;
	float meshHeight = 1;

	[HideInInspector]
	public int sortingLayer = 0;
	private int _sortingLayer;

	[HideInInspector]
	public int orderInLayer = 0;
	private int _orderInLayer = 0;

    public enum WavyPreset{NONE, WavyVertical_LeftAnchor, WavyVertical_RightAnchor,
                                WavyHorizontal_BottomAnchor,WavyHorizontal_TopAnchor  };
    public WavyPreset CurrentPreset;
    

	void OnEnable()
	{
		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		if (sr != null)
		{
			sprite = sr.sprite;
			float scale = Mathf.Max(sprite.textureRect.width, sprite.textureRect.height);
			sortingLayer = sr.sortingLayerID;
			orderInLayer = sr.sortingOrder;
			_tint = tint = sr.color;
			transform.localScale = new Vector3(scale * transform.localScale.x * (sr.flipX ? -1 : 1),
				scale * transform.localScale.y * (sr.flipY ? -1 : 1), 1);
			DestroyImmediate(sr);
		}
		
		mr = GetComponent<MeshRenderer>();
		if (mr == null)
		{
			mr = gameObject.AddComponent<MeshRenderer>();
		}
		mf = GetComponent<MeshFilter>();
		if (mf == null)
		{
			mf = gameObject.AddComponent<MeshFilter>();
		}
		
		SetMeshAndMaterial();
		GenerateMesh();
		UpdateThings();
	}

	void UpdateThings()
	{
		mat.SetFloat("_WaveDirection", waveDirection == waveDirections.Vertical ? 1 : 0);
		switch (staticSide)
		{
			case objSides.None:
				mat.SetFloat("_StaticSide", 0);
				break;
			case objSides.Right:
				mat.SetFloat("_StaticSide", 1);
				break;
			case objSides.Top:
				mat.SetFloat("_StaticSide", 2);
				break;
			case objSides.Left:
				mat.SetFloat("_StaticSide", 3);
				break;
			case objSides.Bottom:
				mat.SetFloat("_StaticSide", 4);
				break;
		}
		mat.SetFloat("_WaveFrequency", waveFrequency);
		mat.SetFloat("_WaveForce", waveForce);
		mat.SetFloat("_WaveSpeed", waveSpeed);
	}
	
	void Update()
	{
		if(	_sprite != sprite || 
			_tint != tint ||
			_divisionsX != divisionsX || 
			_divisionsY != divisionsY || 
			_waveDirection != waveDirection || 
			_staticSide != staticSide || 
			_waveFrequency != waveFrequency || 
			_waveForce != waveForce ||
			_waveSpeed != waveSpeed ||
			_sortingLayer != sortingLayer ||
			_orderInLayer != orderInLayer)
		{
			SetMeshAndMaterial();

			_divisionsX = divisionsX;
			_divisionsY = divisionsY;

			if (_waveDirection != waveDirection)
			{
				mat.SetFloat("_WaveDirection", waveDirection == waveDirections.Vertical ? 1 : 0);
				_waveDirection = waveDirection;
			}
			if (_staticSide != staticSide)
			{
				switch (staticSide)
				{
					case objSides.Top:
						mat.SetFloat("_StaticSide", 2);
						break;
					case objSides.Right:
						mat.SetFloat("_StaticSide", 1);
						break;
					case objSides.Bottom:
						mat.SetFloat("_StaticSide", 4);
						break;
					case objSides.Left:
						mat.SetFloat("_StaticSide", 3);
						break;
					case objSides.None:
						mat.SetFloat("_StaticSide", 0);
						break;
				}
				_staticSide = staticSide;
			}
			_waveFrequency = waveFrequency;
			_waveForce = waveForce;
			_waveSpeed = waveSpeed;
			mat.SetFloat("_WaveFrequency", waveFrequency);
			mat.SetFloat("_WaveForce", waveForce);
			mat.SetFloat("_WaveSpeed", waveSpeed);

			if (_sprite != sprite)
			{
				_sprite = sprite;
				mat.SetTexture("_MainTex", sprite.texture);
				if (sprite != null)
				{
					if (_sprite.rect.width > _sprite.rect.height)
					{
						meshWidth = 1.0f;
						meshHeight = _sprite.rect.height / _sprite.rect.width;
					}
					else
					{
						meshWidth = _sprite.rect.width / _sprite.rect.height;
						meshHeight = 1.0f;
					}
				}else
				{
					meshWidth = 1.0f;
					meshHeight = 1.0f;
				}
			}

			_tint = tint;
			mat.SetColor("_Color", tint);

			if(_sortingLayer != sortingLayer || _orderInLayer != orderInLayer)
			{
				mr.sortingLayerID = sortingLayer;
				mr.sortingOrder = orderInLayer;
				_sortingLayer = sortingLayer;
				_orderInLayer = orderInLayer;
			}
			GenerateMesh();
		}
	}

	void SetMeshAndMaterial()
	{
		if (mesh == null)
		{
			mesh = new Mesh();
			mesh.name = "WavySpriteMesh";
			if (mf.sharedMesh != null)
			{
				DestroyImmediate(mf.sharedMesh);
			}
		}

		if (mf.sharedMesh == null)
		{
			mf.sharedMesh = mesh;
		}

		if (mat == null)
		{
			mat = new Material(Shader.Find("Custom/WavySprite"));
			mat.name = "WavySpriteMaterial";
			if (mr.sharedMaterial != null)
			{
				DestroyImmediate(mr.sharedMaterial);
			}
		}

		if (mr.sharedMaterial == null)
		{
			mr.sharedMaterial = mat;
		}
	}
	
	void GenerateMesh()
	{
		if (_sprite == null)
		{
			return;
		}
		
		int pointsX = divisionsX + 2;
		int pointsY = divisionsY + 2;
		int verticeNum = 0;
		int squareNum = -1;

		vertices.Clear();
		uv0.Clear();
		uv1.Clear();
		colors.Clear();
		
		Vector2 offset = new Vector2(sprite.pivot.x / sprite.rect.width, sprite.pivot.y / sprite.rect.height);
		
		triangles = new int[((pointsX*pointsY)*2)*3];
		for (int y = 0; y < pointsY; y++)
		{
			float yT = (float)y/(pointsY-1);
			float v = Mathf.Lerp(_sprite.rect.min.y, _sprite.rect.max.y, yT);
			
			for (int x = 0; x < pointsX; x++)
			{
				vertices.Add(new Vector3(
					(((float)x/(pointsX-1) - offset.x)) * meshWidth,
					(((float)y/(pointsY-1) - offset.y)) * meshHeight,
					0f
				) / sprite.pixelsPerUnit);
				
				float xT = (float)x/(pointsX-1);
				float u = Mathf.Lerp(_sprite.rect.min.x, _sprite.rect.max.x, xT);
				uv0.Add(new Vector3(u / _sprite.texture.width, v / _sprite.texture.height, 0.0f));
				uv1.Add(new Vector3(yT, xT, 0.0f));

				//Add triangles
				if (x > 0 && y > 0)
				{
					verticeNum = x + (y*pointsX);
					squareNum++;
					triangles[squareNum*6] = verticeNum -pointsX - 1;
					triangles[squareNum*6+1] = verticeNum - 1;
					triangles[squareNum*6+2] = verticeNum;
					triangles[squareNum*6+3] = verticeNum;
					triangles[squareNum*6+4] = verticeNum - pointsX;
					triangles[squareNum*6+5] = verticeNum - pointsX - 1;
				}
			}
		}
		
		mesh.Clear();
		mesh.SetVertices(vertices);
		mesh.SetUVs(0, uv0);
		mesh.SetUVs(1, uv1);
		mesh.SetColors(colors);
		mesh.SetTriangles(triangles, 0);
	}

    public void LoadNewPreset(WavySpriteVariablePresets newPreset)
    {
        divisionsX = newPreset.DivisionsX;
        divisionsY = newPreset.DivisionsY;
        waveDirection = newPreset.WaveDirection;
        staticSide = newPreset.StaticSide;
        waveFrequency = newPreset.WaveFrequency;
        waveForce = newPreset.WaveForce;
        waveSpeed = newPreset.WaveSpeed;
    }

    public void LoadNewPreset(int DivisionsX, int DivisionsY, waveDirections WaveDirection, objSides StaticSide, float WaveFrequency, float WaveForce, float WaveSpeed)
    {
        divisionsX = DivisionsX;
        divisionsY = DivisionsY;
        waveDirection = WaveDirection;
        staticSide = StaticSide;
        waveFrequency = WaveFrequency;
        waveForce = WaveForce;
        waveSpeed = WaveSpeed;
    }
}

[System.Serializable]
public class WavySpriteVariablePresets
{
    public string Label;
    [Range(0, 30)]
    public int DivisionsX = 2;
    [Range(0, 30)]
    public int DivisionsY =2;
    public WavySprite.waveDirections WaveDirection = WavySprite.waveDirections.Vertical;
    public WavySprite.objSides StaticSide = WavySprite.objSides.None;

    [Range(-10, 10)]
    public float WaveFrequency =10f;

    [Range(0,  1)]
    public float WaveForce= 0.03f;

    [Range(0, 10)]
    public float WaveSpeed = 0.5f;
}

