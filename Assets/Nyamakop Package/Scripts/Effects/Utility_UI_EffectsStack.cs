﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Utility_UI_EffectsStack : MonoBehaviour {

    private MaskableGraphic _graphic; //Can be text, image etc.


    /****Color****/

        //Oscillate color
    private Color _colorA, _colorB, _returnColor;
    private float _oscillateColorPeriod;

    /***Angle***/

    //Angle Oscillate
    private float _angleOscillatePeriod, _angleIntensity;
    private Vector3 _startAngle;

	// Use this for initialization
	void Awake () {
        if(GetComponent<MaskableGraphic>() != null)_graphic = GetComponent<MaskableGraphic>();
	}

    private void Update()
    {
        if (_isFlashingColor) FlashColorChangeUpdate();
        if (_isAngling) AngleOscillateUpdate();
    }


    //Scale
    private Tween _curScaleTween;
    public void ScalePulse( Vector3 pulseSize, float periodOut, float periodBack,  Ease easeTypeOut = Ease.InOutElastic, Ease easeTypeBack = Ease.Linear)
    {
        var startScale = _graphic.rectTransform.localScale;
        if (_curScaleTween != null && !_curScaleTween.IsComplete()) _curScaleTween.Complete();
        _curScaleTween =_graphic.rectTransform.DOScale(pulseSize, periodOut).SetEase(easeTypeOut).OnComplete(() => {
            _graphic.rectTransform.DOScale(startScale, periodBack).SetEase(easeTypeBack);
        });
    }

    //Color
    private bool _isFlashingColor;

    //Oscilliate color
    //Call this
    public void FlashColorChangeStart(float FlashColorPeriod, float oscillateColorPeriod, Color ColorA, Color ColorB, Color ReturnColor)
    {
        _oscillateColorPeriod = oscillateColorPeriod;
        _colorA = ColorA;
        _colorB = ColorB;
        _returnColor = ReturnColor;

        Sequence seq = DOTween.Sequence();
        seq.AppendCallback(() =>
        {
            _isFlashingColor = true;
        });
        seq.AppendInterval(FlashColorPeriod);
        seq.AppendCallback(() =>
        {
            _isFlashingColor = false;
            _graphic.color = ReturnColor;
        });
    }

    private void FlashColorChangeUpdate()
    {
        _graphic.color = UtilityFunctions.BobWaveColors(_colorA, _colorB, _oscillateColorPeriod, 0f);
    }


    //Angle
    private bool _isAngling;

    public void AngleOscillateStart(float angIntensity, float angOscillatePeriod, float oscillatePeriod)
    {
        _startAngle = _graphic.rectTransform.localEulerAngles;
        _angleIntensity = angIntensity;
        _angleOscillatePeriod = angOscillatePeriod;

        Sequence seq = DOTween.Sequence();
        seq.AppendCallback(() =>
        {
            _isAngling = true;
        });
        seq.AppendInterval(oscillatePeriod);
        seq.AppendCallback(() =>
        {
            _isAngling = false;
            _graphic.rectTransform.localEulerAngles = _startAngle;
        });
    }

    private void AngleOscillateUpdate()
    {
        _graphic.rectTransform.localEulerAngles = new Vector3(0f, 0f, UtilityFunctions.BobWave(-_angleIntensity, _angleIntensity, _angleOscillatePeriod, 0f));
    }
}
