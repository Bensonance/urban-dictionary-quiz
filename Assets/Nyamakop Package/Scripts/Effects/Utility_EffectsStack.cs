﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Utility_EffectsStack : MonoBehaviour
{

    public SpriteRenderer SpriteHolder;
    private Material _renderBackup;

    [Header("Oscillate Color")]
    public Color StandardColor, DamagedColor = Color.white, HealColor = Color.green;
    public float DamageFlashPeriod, FlashOscillatePeriod;
    private bool _isFlashing;

    [Header("Flash Color")]
    public float DamageColorHoldPeriod;
    private Sequence _flashColor;

    [Header("Death Effects")]
    public bool MakeNewColor;
    public Color DeathColor;
    public bool DeathMotion;
    public float DeathMotionX, DeathMotionY;
    private Rigidbody _mover;
    public bool TurnOffComponents;
    public MonoBehaviour[] DeathComponents;
    public SpriteRenderer[] OtherAffectedSprites;
    public SpriteRenderer[] TurnedOffSprites;
    public bool RotateDeath;
    public float RotateGoal, RotateTime;

    [Header("Shudder Effects")]
    public Vector2 ShudderRange;
    public Vector3 ShudderDimensionPeriods;
    public float ShudderPeriod;
    private bool _isShuddering;
    private Vector3 _startScale;

    private void Awake()
    {
        if (SpriteHolder == null)
        {
            if (GetComponent<SpriteRenderer>() != null)
            {
                SpriteHolder = GetComponent<SpriteRenderer>();
            }
            else
            {
                if(GetComponent<Renderer>() != null)_renderBackup = GetComponent<Renderer>().material;
            }
        }
        if(_renderBackup != null || SpriteHolder != null)StandardColor = SpriteHolder == null ? _renderBackup.color : SpriteHolder.color;
        if(GetComponent<Rigidbody>()!= null)_mover = GetComponent<Rigidbody>();
    }

    public void Update()
    {
        if (_isFlashing) FlashColorChangeUpdate();
        if (_isShuddering) ShudderUpdate();
    }

    //Oscillate color
    public void FlashColorChangeUpdate()
    {
        SpriteHolder.color = UtilityFunctions.BobWaveColors(StandardColor, DamagedColor, FlashOscillatePeriod, 0f);
    }


    public void FlashColorChangeStart()
    {
        Sequence seq = DOTween.Sequence();
        seq.AppendCallback(() =>
        {
            _isFlashing = true;
        });
        seq.AppendInterval(DamageFlashPeriod);
        seq.AppendCallback(() =>
        {
            _isFlashing = false;
        });
    }

    //Flash color
    public void HealColorFlash()
    {
        HoldColorChange(HealColor);
    }

    public void DamageColorFlash()
    {
        HoldColorChange(DamagedColor);
    }

    public void HoldColorChange(Color changedcolor)
    {
        Debug.Log("Yeah");
        Sequence seq = DOTween.Sequence();
        seq.AppendCallback(() =>
        {
            if (SpriteHolder == null)
            {
                _renderBackup.color = changedcolor;
            }
            else
            {
                SpriteHolder.color = changedcolor;
            }
            
        });
        seq.AppendInterval(DamageColorHoldPeriod);
        seq.AppendCallback(() =>
        {
            if (SpriteHolder == null)
            {
                _renderBackup.color = StandardColor;
            }
            else
            {
                SpriteHolder.color = StandardColor;
            }
        });

        _flashColor = seq;
    }

    //Shake
    public void CameraShakeSmall()
    {
        CameraBehaviour_Parent.Instance.ShakeCamera(0.0085f, 0.25f, false);
    }

    public void CameraShakeMedium()
    {
        CameraBehaviour_Parent.Instance.ShakeCamera(8f, 0.25f, true);
    }

    //Death
    public void DeathEffects()
    {
        if (_flashColor != null) _flashColor.Complete();
        SpriteHolder.color = DeathColor;
        if (_mover != null) _mover.velocity = new Vector2(UtilityFunctions.RandomSign(50) * DeathMotionX, DeathMotionY);
        foreach (MonoBehaviour c in DeathComponents) c.enabled = false;
        foreach (SpriteRenderer s in OtherAffectedSprites) s.color = DeathColor;
        foreach (SpriteRenderer s in TurnedOffSprites) s.enabled = false;
        if (RotateDeath) transform.DORotate(new Vector3(0f, 0f, RotateGoal), RotateTime);

    }

    //Scale Shudder
    public void ShudderStart()
    {
        Sequence seq = DOTween.Sequence();
        _startScale = transform.localScale;
        seq.AppendCallback(() =>
        {
            _isShuddering = true;
        });
        seq.AppendInterval(ShudderPeriod);
        seq.AppendCallback(() =>
        {
            _isShuddering = false;
            transform.localScale = _startScale;
        });
    }

    public void ShudderUpdate()
    {
        transform.localScale = _startScale +new Vector3(
            UtilityFunctions.BobWave(ShudderRange.x, ShudderRange.y, ShudderDimensionPeriods.x, 0f),
            UtilityFunctions.BobWave(ShudderRange.x, ShudderRange.y, ShudderDimensionPeriods.y, 0f),
            UtilityFunctions.BobWave(ShudderRange.x, ShudderRange.y, ShudderDimensionPeriods.z, 0f)
            );
    }
}
