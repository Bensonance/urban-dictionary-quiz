﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public struct BobProps
{
	public float from;
	public float to;
	public float duration;
	public float offset;
}

public class BobRectTransform : MonoBehaviour
{
	public BobProps XBobProps;
	public BobProps YBobProps;

    public bool BobScale;
    public BobProps XScaleBobProps;
    public BobProps YScaleBobProps;
    

    private RectTransform _rectTransform;
    private Vector2 _startPos, _startScale;

    void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _startPos = _rectTransform.anchoredPosition;
        _startScale = _rectTransform.localScale;
    }
	
    void Update()
    {
        var pos = new Vector2(UtilityFunctions.BobWave(XBobProps.from, XBobProps.to, XBobProps.duration, XBobProps.offset), 
                UtilityFunctions.BobWave(YBobProps.from, YBobProps.to, YBobProps.duration, YBobProps.offset));
        _rectTransform.anchoredPosition = _startPos + pos;

        if (!BobScale) return;
        var scale = new Vector2(UtilityFunctions.BobWave(XScaleBobProps.from, XScaleBobProps.to, XScaleBobProps.duration, XScaleBobProps.offset),
                UtilityFunctions.BobWave(YScaleBobProps.from, YScaleBobProps.to, YScaleBobProps.duration, YScaleBobProps.offset));

        _rectTransform.localScale = _startScale + scale;
    }
}
