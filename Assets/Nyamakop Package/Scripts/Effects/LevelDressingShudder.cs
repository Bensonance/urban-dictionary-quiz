﻿using UnityEngine;
using System.Collections;

public class LevelDressingShudder : MonoBehaviour {

    private Vector2 _shudderRange;
    private Vector3 _curShudderOffset;
    private Vector3 _startPostion;

    private float _curTime;

    private float _lerpSpeed;

	// Use this for initialization
	void Start () {
        _startPostion = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        UpdateShudder();
	}

    private void UpdateShudder()
    {
        if(_curTime <= 0)
        {
            _shudderRange = Vector2.Lerp(_shudderRange, Vector2.zero, _lerpSpeed);
        }
        else
        {
            _curTime -= Time.deltaTime;
        }
        _curShudderOffset = new Vector2(Random.Range(-_shudderRange.x, _shudderRange.x), Random.Range(-_shudderRange.y, _shudderRange.y));
        transform.position = _startPostion + _curShudderOffset;
    }

    private void Shudder(Vector2 shudderOffset, float shudderTime, bool lerpDown, float lerpSpeed)
    {
        _shudderRange = shudderOffset;
        _curTime = shudderTime;
        _lerpSpeed = lerpSpeed;
        transform.GetChild(0).GetComponent<ParticleSystem>().Play();
        if(!lerpDown) _lerpSpeed = 1;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        Debug.Log("yeah");
        if(coll.tag == "Player")
        {
            Shudder(new Vector2(16f, 0f), 0.35f, true, 0.4f);
        }
    }
}
