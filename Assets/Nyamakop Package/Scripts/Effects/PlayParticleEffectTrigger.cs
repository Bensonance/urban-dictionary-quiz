﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayParticleEffectTrigger : MonoBehaviour
{
	public ParticleSystem[] ParticleSystemsToPlay;
    public bool PlayOnce = true;
    private bool _hasPlayed = false;

    void Start()
    {
		GetComponent<SpriteRenderer>().enabled = false;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
    	if (coll.tag != "Player" || (PlayOnce && _hasPlayed))
    		return;

    	for (int i = 0; i < ParticleSystemsToPlay.Length; i++)
    	{
    		ParticleSystemsToPlay[i].Play();

            // play sound event
    	}

        _hasPlayed = true;
    }
}
