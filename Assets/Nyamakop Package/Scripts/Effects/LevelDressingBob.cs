﻿using UnityEngine;
using System.Collections;

public class LevelDressingBob : MonoBehaviour {

    public bool BobX, BobY;
    private Vector3 _startPosition;
    public Vector2 BobXValues;
    public float BobXTime;
    private float _offset;
	// Use this for initialization
	void Start () {
        _startPosition = transform.position;
        _offset = Random.Range(-1f, 1f);
	}
	
	// Update is called once per frame
	void Update () {
        float bobX = UtilityFunctions.BobWave(BobXValues.x, BobXValues.y, BobXTime, _offset);
        transform.position = _startPosition + new Vector3(bobX, 0f, 0f);
    }
}
