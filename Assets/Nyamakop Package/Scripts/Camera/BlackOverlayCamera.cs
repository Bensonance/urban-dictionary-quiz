﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Nyamakop;

public class BlackOverlayCamera : Singleton<BlackOverlayCamera>
{

    private Camera _camera;
    private Camera _mainCamera;
    private Transform _mainCameraTransform;
    private bool _initialized;

    private Image _blackOverlay;
    public Color BlackOverlayColor
    {
        get { return _blackOverlay == null ? Color.clear : _blackOverlay.color; }
        set { _blackOverlay.color = value; }
    }

    void Start()
    {
        _blackOverlay = GetComponentInChildren<Image>();
        _camera = GetComponent<Camera>();
        _mainCamera = Camera.main;
        _mainCameraTransform = _mainCamera.transform;
        _initialized = true;
        BlackOverlayColor = Color.clear;
        
    }

    public void FadeOut()
    {
        StartCoroutine(FadeOut(2f));
    }

    public void FadeIn()
    {
        StartCoroutine(FadeIn(2f));
    }

    public void ManualUpdate()
    {
        if (!_initialized)
            Start();
        
        transform.position = _mainCameraTransform.position;
        _camera.orthographicSize = Camera.main.orthographicSize;
    }

    public IEnumerator FadeOut(float time, bool showLoadingSpinner = true)
    {
        float t = 0f;

        while (t < time)
        {
            float s = t / time;
            BlackOverlayColor = Color.Lerp(Color.clear, Color.black, s);
            t += Time.deltaTime;
            yield return null;
        }

        BlackOverlayColor = Color.black;
        
    }

    public IEnumerator FadeIn(float time, bool hideLoadingSpinner = true)
    {
        float t = 0f;

        while (t < time)
        {
            float s = t / time;
            BlackOverlayColor = Color.Lerp(Color.black, Color.clear, s);
            t += Time.deltaTime;
            yield return null;
        }

        BlackOverlayColor = Color.clear;
        
    }

    void LevelFinishedLoading(Scene scene, LoadSceneMode mode)
	{
        if (!_initialized)
            Start();
	}

    void SceneUnloaded(Scene sc)
    {
        _initialized = false;
    }

	void OnEnable()
	{
		SceneManager.sceneLoaded += LevelFinishedLoading;
        SceneManager.sceneUnloaded += SceneUnloaded;
	}

	void OnDisable()
	{
		SceneManager.sceneLoaded -= LevelFinishedLoading;
        SceneManager.sceneUnloaded -= SceneUnloaded;
	}
}
