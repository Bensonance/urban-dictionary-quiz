﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera3DVignette : CameraBehaviour_Parent {

    public Transform LookTarget;
    public Vector3 LookOffset;

    private float _distanceFromTarget;
    private float _currentAngle = -45f;
    public float AngleRotateStep;

	// Use this for initialization
	void Start () {
        _distanceFromTarget = Vector3.Distance(LookTarget.position, transform.position);
	}

    private void Update()
    {
        base.Update();
        RotateCamera();
    }

    // Update is called once per frame
    void LateUpdate () {
        transform.LookAt(LookTarget.position + LookOffset);
        transform.position = transform.position + (Vector3)_shakeVector;
        DistanceAndRotation();
    }

    void DistanceAndRotation()
    {
        Vector3 refPos = LookTarget.transform.position;
        refPos.y = 0f;
        transform.position = Vector3.Lerp(transform.position,  refPos+ new Vector3(Mathf.Cos(Mathf.Deg2Rad * _currentAngle) * _distanceFromTarget,
                                        transform.position.y,
                                        Mathf.Sin(Mathf.Deg2Rad * _currentAngle) * _distanceFromTarget), 0.3f) +(Vector3)_shakeVector;
    }

    void RotateCamera()
    {
        if(Input.GetKey(KeyCode.Q))
        {
            _currentAngle -= AngleRotateStep;
        }else if(Input.GetKey(KeyCode.E))
        {
            _currentAngle += AngleRotateStep;
        }
    }
}
