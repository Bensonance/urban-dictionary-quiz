﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nyamakop;

public class CameraBehaviour_Parent : Singleton<CameraBehaviour_Parent> {

    //Shake
    protected Vector2 _shakeVector;
    protected Vector2 _shakeRange;
    protected float _curShakeTime;
    protected bool _isShaking = true;

    public void Update()
    {
        if (_isShaking) ShakeCountdown();
    }

    //Shaking
    protected void ShakeCountdown()
    {
        if (_curShakeTime <= 0)
        {
            _isShaking = false;
            _shakeRange = Vector2.zero;
            _shakeVector = Vector2.zero;
        }
        else
        {
            _shakeVector = new Vector2(UtilityFunctions.RandomValueInRange(_shakeRange), UtilityFunctions.RandomValueInRange(_shakeRange));
            _curShakeTime -= Time.deltaTime;
        }
    }

    public void ShakeCamera(float shakeVariation, float shakePeriod, bool addToShake = false)
    {
        _curShakeTime = shakePeriod;
        var shake = new Vector2(-shakeVariation, shakeVariation);
        _shakeRange = addToShake ? _shakeRange + shake : shake;
        _isShaking = true;
    }
}
