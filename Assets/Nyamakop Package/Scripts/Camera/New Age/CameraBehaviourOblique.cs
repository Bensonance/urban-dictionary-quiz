﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nyamakop;

public class CameraBehaviourOblique : CameraBehaviour_Parent {

    public Transform Target;
    public Vector3 Offset;
    public float ClampCheck;
    private Vector3 _refSpeed;
    


    private void LateUpdate()
    {
        Vector3 centered = Vector3.SmoothDamp(transform.position, Target.transform.position, ref _refSpeed,  ClampCheck);
        transform.position = centered + (Vector3)_shakeVector + Offset;
    }

    
}
