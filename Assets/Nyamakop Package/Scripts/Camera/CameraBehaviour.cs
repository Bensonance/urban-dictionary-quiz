﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Gamelogic;
using Com.LuisPedroFonseca.ProCamera2D;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class CameraBehaviour : MonoBehaviour
{
    public bool ResetCameraZoom;
    
    public bool FocusOnPlayer;

    private GameObject _playerCamera;
    private BlackOverlayCamera _blackOverlayCamera;

    [HideInInspector] public Camera BackgroundCamera;

    ProCamera2DPositionAndSizeOverrider _overridePosition;

    void Awake()
    {
        _blackOverlayCamera = FindObjectOfType<BlackOverlayCamera>();

        ProCamera2D.Instance.UpdateType = UpdateType.ManualUpdate;

        BackgroundCamera = transform.Find("ParallaxCamera (0.25)").GetComponent<Camera>();

        var positionAndSizeOverrider = ProCamera2D.Instance.GetComponent<ProCamera2DPositionAndSizeOverrider>();
        _overridePosition = positionAndSizeOverrider == null ? gameObject.AddComponent<ProCamera2DPositionAndSizeOverrider>() : positionAndSizeOverrider;
        // _overridePosition.hideFlags = HideFlags.HideInInspector;
    }

    public void ShakeCamera(int preset)
    {
        ProCamera2DShake.Instance.StopShaking();
        ProCamera2DShake.Instance.ShakeUsingPreset(preset);
    }

    public void OffsetCamera(float duration, Vector2 strength, float initialAngle = -1f, float smoothness = 0.249f)
    {
        ProCamera2DShake.Instance.Shake(duration, strength, 1, 0, initialAngle, Vector3.zero, smoothness);
    }

    public void Shake(float duration, Vector2 strength, int vibrato = 10, float randomness = 0.1f, float initialAngle = -1f, Vector3 rotation = default(Vector3), float smoothness = 0.1f)
    {
        ProCamera2DShake.Instance.Shake(duration, strength, vibrato, randomness, initialAngle, rotation, smoothness);
    }

    
    public void UpdateCamera(float dt)
    {
        ProCamera2D.Instance.Move(dt);
        _blackOverlayCamera.ManualUpdate();

    }

    public static void FocusCameraOnPlayer(bool centerOnPlayer = true)
    {
        
        Debug.Log("added player: " + UnityEngine.StackTraceUtility.ExtractStackTrace());
        ProCamera2D.Instance.RemoveAllCameraTargets();
        //ProCamera2D.Instance.AddCameraTarget(CharacterBehaviour.Instance.PlayerAnchor);

        ProCamera2D.Instance.FollowHorizontal = true;
        ProCamera2D.Instance.FollowVertical = true;

        if (centerOnPlayer)
            ProCamera2D.Instance.CenterTarget();
    }

    public static void AddPlayerToCamera()
    {
        //ProCamera2D.Instance.AddCameraTarget(CharacterBehaviour.Instance.PlayerAnchor);
        ProCamera2D.Instance.FollowHorizontal = ProCamera2D.Instance.FollowVertical = true;
    }

    public static void AddCameraTarget(Transform target, float followSmoothness = 0.15f)
    {
        ProCamera2D.Instance.AddCameraTarget(target);
        ProCamera2D.Instance.FollowHorizontal = ProCamera2D.Instance.FollowVertical = true;
        ProCamera2D.Instance.HorizontalFollowSmoothness = ProCamera2D.Instance.VerticalFollowSmoothness = followSmoothness;
    }

    public static void RemoveAllCameraTargets()
    {
        ProCamera2D.Instance.RemoveAllCameraTargets();
        ProCamera2D.Instance.FollowHorizontal = ProCamera2D.Instance.FollowVertical = false;
    }

    public static void SetCameraPosition(Vector3 pos)
    {
        ProCamera2D.Instance.MoveCameraInstantlyToPosition(pos);
    }

    public void SetCameraBackgroundColor(Color col)
    {
        BackgroundCamera.backgroundColor = col;
    }

    public void ResetCameraSize()
    {
        var cameras = Camera.main.GetComponentsInChildren<Camera>();
        foreach (var c in cameras)
        {
            c.orthographicSize = 540f;
        }

        var pos = this.transform.position;
        pos.z = -100;
        Camera.main.transform.position = pos;
    }

    public static void SetCameraZoom(float zoom)
    {
        var cameras = Camera.main.GetComponentsInChildren<Camera>();
        foreach (var c in cameras)
        {
            c.orthographicSize = zoom;
        }
    }

    public void SetViewOnPlayer()
    {
        var pos = GameObject.Find("Player").transform.Find("Anchor").position;
        pos.z = -100f;
        transform.position = pos;
    }

    public void TakeControlOfCamera(Vector3 position, float size = -1f)
    {
        _overridePosition.Enabled = true;
        _overridePosition.OverridePosition = position;
        _overridePosition.OverrideSize = size;
    }

    public void GiveBackControlOfCamera()
    {
        _overridePosition.Enabled = false;
    }

    public static void SetImmediatelyToTrigger(ProCamera2DTriggerZoom zoom = null, ProCamera2DTriggerInfluence influence = null)
    {
        if (zoom != null)
        {
            ProCamera2D.Instance.UpdateScreenSize(zoom.TargetZoom, 0, EaseType.Linear);
        }
        else
        {
            Debug.LogError("No zoom trigger component on passed object");
        }

        if (influence != null)
        {
            ProCamera2D.Instance.MoveCameraInstantlyToPosition(influence.transform.position);
        }
        else
        {
            Debug.LogError("No influence trigger component on passed object");
        }
    }

    public static void SetImmediatelyToTrigger(GameObject triggerHolder)
    {
        var zoom = triggerHolder.GetComponent<ProCamera2DTriggerZoom>();
        if (zoom == null)
            Debug.LogError("No zoom trigger component on passed object");
        
        ProCamera2D.Instance.UpdateScreenSize(zoom.TargetZoom, 0, EaseType.Linear);

        var influence = triggerHolder.GetComponent<ProCamera2DTriggerInfluence>();
        if (influence == null)
            Debug.LogError("No influence trigger component on passed object");
        ProCamera2D.Instance.MoveCameraInstantlyToPosition(influence.transform.position);

    }
}
