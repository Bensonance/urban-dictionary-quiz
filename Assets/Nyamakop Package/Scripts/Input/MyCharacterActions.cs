﻿using InControl;

public class MyCharacterActions : PlayerActionSet
{
    public PlayerAction Left;
    public PlayerAction Right;
    public PlayerAction Up;
    public PlayerAction Down;
    public PlayerAction Jump;
    public PlayerAction Dash;
    public PlayerAction Submit;
    public PlayerAction Cancel;
    public PlayerAction Start;
    public PlayerAction OpenGameUI;
    public PlayerAction ReformPulse;
    public PlayerAction Menu;

    public PlayerAction Input1, Input2, Input3, Input4;

    public PlayerAction Player1Default, Player2Default;

    public PlayerOneAxisAction LeftStickX;
    public PlayerOneAxisAction LeftStickY;
    public PlayerTwoAxisAction LeftStick;

    public PlayerAction Suck, Spew;

    public MyCharacterActions()
    {
        Left = CreatePlayerAction("Move Left");
        Right = CreatePlayerAction("Move Right");
        Up = CreatePlayerAction("Move Up");
        Down = CreatePlayerAction("Move Down");
        Dash = CreatePlayerAction("Dash");
        Jump = CreatePlayerAction("Jump");
        Submit = CreatePlayerAction("Submit");
        Cancel = CreatePlayerAction("Cancel");
        Start = CreatePlayerAction("Start");
        OpenGameUI = CreatePlayerAction("Open Game UI");
        ReformPulse = CreatePlayerAction("Reform Pulse");
        Menu = CreatePlayerAction("Menu");
        Suck = CreatePlayerAction("Suck");
        Spew = CreatePlayerAction("Spew");

        LeftStickX = CreateOneAxisPlayerAction(Left, Right);
        LeftStickY = CreateOneAxisPlayerAction(Down, Up);
        LeftStick = CreateTwoAxisPlayerAction(Left, Right, Down, Up);

        Input1 = CreatePlayerAction("Option 1");
        Input2 = CreatePlayerAction("Option 2");
        Input3 = CreatePlayerAction("Option 3");
        Input4 = CreatePlayerAction("Option 4");

        Player1Default = CreatePlayerAction("Player 1 Default");
        Player2Default = CreatePlayerAction("Player 2 Default");
    }

    public void Initialize()
    {
        Left.AddDefaultBinding(Key.LeftArrow);
        Left.AddDefaultBinding(InputControlType.DPadLeft);
        Left.AddDefaultBinding(InputControlType.LeftStickLeft);
        Left.AddDefaultBinding(Key.A);

        Right.AddDefaultBinding(Key.RightArrow);
        Right.AddDefaultBinding(InputControlType.DPadRight);
        Right.AddDefaultBinding(InputControlType.LeftStickRight);
        Right.AddDefaultBinding(Key.D);

        Up.AddDefaultBinding(Key.UpArrow);
        Up.AddDefaultBinding(InputControlType.DPadUp);
        Up.AddDefaultBinding(InputControlType.LeftStickUp);
        Up.AddDefaultBinding(Key.W);

        Down.AddDefaultBinding(Key.DownArrow);
        Down.AddDefaultBinding(InputControlType.DPadDown);
        Down.AddDefaultBinding(InputControlType.LeftStickDown);
        Down.AddDefaultBinding(Key.S);

        Dash.AddDefaultBinding(Key.Space);
        Dash.AddDefaultBinding(InputControlType.RightTrigger);
        Dash.AddDefaultBinding(InputControlType.Action3);

        Jump.AddDefaultBinding(Key.UpArrow);
        Jump.AddDefaultBinding(InputControlType.Action1);

        OpenGameUI.AddDefaultBinding(Key.C);
        OpenGameUI.AddDefaultBinding(InputControlType.Action4);

        ReformPulse.AddDefaultBinding(Key.Alt);
        ReformPulse.AddDefaultBinding(InputControlType.Action2);

        Menu.AddDefaultBinding(Key.Escape);
        Menu.AddDefaultBinding(InputControlType.Command);

        Submit.AddDefaultBinding(Key.Return);
        Submit.AddDefaultBinding(Key.Space);

        Suck.AddDefaultBinding(Mouse.RightButton);
        Spew.AddDefaultBinding(Mouse.LeftButton);

        Input1.AddDefaultBinding(Key.Z);
        Input1.AddDefaultBinding(InputControlType.Action1);

        Input2.AddDefaultBinding(Key.X);
        Input2.AddDefaultBinding(InputControlType.Action2);

        Input3.AddDefaultBinding(Key.C);
        Input3.AddDefaultBinding(InputControlType.Action3);

        Input4.AddDefaultBinding(Key.V);
        Input4.AddDefaultBinding(InputControlType.Action4);

        Player1Default.AddDefaultBinding(Key.LeftControl);
        Player1Default.AddDefaultBinding(InputControlType.Action1);

        Player2Default.AddDefaultBinding(Key.RightControl);
        Player2Default.AddDefaultBinding(InputControlType.Action1);

#if UNITY_STANDALONE
        Submit.AddDefaultBinding(InputControlType.Action1);
#endif
#if UNITY_SWITCH
        Submit.AddDefaultBinding(InputControlType.Action2);
#endif
        Start.AddDefaultBinding(Key.Return);
        Start.AddDefaultBinding(InputControlType.Start);

        Cancel.AddDefaultBinding(Key.Escape);
#if UNITY_STANDALONE
        Cancel.AddDefaultBinding(InputControlType.Action2);
#endif
#if UNITY_SWITCH
        Cancel.AddDefaultBinding(InputControlType.Action1);
#endif
    }
}